﻿using System;
using UnityEngine;

[Serializable]

public class AttackSpot : MonoBehaviour
{
    public bool _isRange;
    public GameObject _player;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            _isRange = true;
            _player = collision.gameObject;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            _isRange = false;
            _player = null;
        }
    }


}
