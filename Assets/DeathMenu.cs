﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DeathMenu : MonoBehaviour
{

    public GameObject canvaRespawn;
    //private Vector3 respawnPoint;
    public GameObject m_btnGroup;
    private List<Button> m_btns;

    private void Awake()
    {
        m_btns = new List<Button> { };
    }

    private void Start()
    {
        for (int i = 0; i < m_btnGroup.transform.childCount; i++)
        {
            m_btns.Add(m_btnGroup.transform.GetChild(i).gameObject.GetComponent<Button>());
        }
        //respawnPoint = Player.instance.gameObject.transform.position; // Set the first respawn point by the first position on the player
        SetButtons();
    }


    private Button GetButtonByName(string name)
    {
        foreach (Button btn in m_btns)
        {
            if(btn.name.Contains(name))
            {
                return btn;
            }
        }
        return null;
    }

    public void SetButtons()
    {
        GetButtonByName("Respawn").onClick.AddListener(() => RespawnLastCampFire());
        GetButtonByName("Quit").onClick.AddListener(() => QuitGame());
    }


    private void RespawnLastCampFire()
    {
        // If none camp fire is activate, we respawn on first position in the game
        if (!canvaRespawn.GetComponent<CampFireTeleport>().IsCampFireActivate() && canvaRespawn.GetComponent<CampFireTeleport>().GetLastCampFire() == null)
        {
            Player.instance.gameObject.transform.position = Player.instance.m_initiateSpawn;
        }
        else
        {
            Player.instance.gameObject.transform.position = canvaRespawn.GetComponent<CampFireTeleport>().GetLastCampFire().transform.position;
        }
        RespawnPlayer();
        GameManager.instance.GetMenuManager().CloseMenu();
    }

    private void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single); // Load current scene
        //panelFallDetector.SetActive(false);
        Time.timeScale = 1; // Restart game
    }

    private void QuitGame()
    {
        Debug.Log("return loby");
        SceneManager.LoadScene("TitlePage"); // Load first scene
    }

    private void RespawnPlayer()
    {
        if (!Player.instance.IsLife())
        {
            Player.instance.setInvincibility();
            Player.instance.FullHeal();
        }
    }
}
