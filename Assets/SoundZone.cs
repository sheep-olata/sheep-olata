﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundZone : MonoBehaviour
{
    [SerializeField] FootstepSet _set;

    // Start is called before the first frame update

    void Start()
    {
        if(_set == null)//safety check
        {
            Debug.LogError("The SoundZone named "+this.gameObject.name+" at position "+transform.position+" has no footstepset, so it will be deleted.");
            Destroy(this.gameObject);
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))//does the gameobject have the "Player" tag?
        {
            collision.gameObject.GetComponent<PlayerMovement>().ChangeFootstepSet(_set); //calls function to change current soundset
            Debug.Log("Current soundset changed to " + _set.SetName);
        }
    }
}
