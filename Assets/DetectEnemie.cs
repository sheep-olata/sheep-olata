﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectEnemie : MonoBehaviour
{
    public bool _isRange = false;
    public GameObject _enemie;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemie"))
        {
            try
            {
                if (collision.GetComponent<Enemie>() != null || collision.GetComponent<Boss>() != null)
                {
                    _isRange = true;
                    _enemie = collision.gameObject;
                }
            }
            catch
            {
                print("la cible n'est pas un enemie");
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemie"))
        {
            _isRange = false;
            _enemie = null;
        }
    }

}
