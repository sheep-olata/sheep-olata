#################################################################################
# Project tree organization						 	#
#################################################################################

> Animations

the animation folder will contain all animations of the project, organized by theme
as main characters, ennemies, decor...
 
> Materials

materials will regroup materials as textures materials, skybox materials...

> Medias

media is the folder that will regroup all custom medias stuffs 
(audio soundtrack as damage taken, imported fonts, images
/!\Needs to be organized, new muscis for the game)

	Audios
	Fonts
	Images
	Musics

> Prefabs

prefabs are custom packages every imported object needs to have his own prefab

> Scenes

regroups levels of the game and scenes

	Levels

> Script

C# scripts and others

> Shaders

regroups objects shaders
 
> Textures

regroups textures images for the game