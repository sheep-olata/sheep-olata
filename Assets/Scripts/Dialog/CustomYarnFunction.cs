﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Yarn.Unity;

namespace Assets.Scripts.Dialog
{
    class CustomYarnFunction : MonoBehaviour
    {
        [SerializeField] DialogueRunner dialogueRunner;
        // a list of all the Yarn node names we have visited
        List<string> _items = new List<string>(); // this could be a HashSet if we want more efficient search


        // the function we will call from the Yarn script
        public bool WasItemInInventory(string item)
        {
            return _items.Contains(item);
        }
        void Start()
        {
            CheckItem();
            DeleteAllItems();
            DeleteItem();
            AddItem();
        }

        /// <summary>
        /// Check if one item exixt in this inventory
        /// </summary>
        private void CheckItem()
        {
            dialogueRunner.AddFunction("checkItem", 1, delegate (Yarn.Value[] parameters)
            {
                var inventoryForQuest = getInventoryForQuest(parameters);

                if (inventoryForQuest.isItemExist)
                {
                    // Return true
                    return true;
                }

                return false;
            });
        }

        /// <summary>
        /// Delete all item in inventory
        /// </summary>
        private void DeleteAllItems()
        {
            dialogueRunner.AddFunction("deleteAllItems", 1, delegate (Yarn.Value[] parameters)
            {
                var inventoryForQuest = getInventoryForQuest(parameters);

                if (inventoryForQuest.isItemExist)
                {
                    var index = inventoryForQuest.index;
                    // Clear all items in this list
                    inventoryForQuest.inventory.itemsCasesInventory[index].Clear();
                    // Update ui interface to clear inventory case
                    FindObjectOfType<Inventaire>().HideCaseInventory(FindObjectOfType<Inventaire>()._inventoryCasesRight[index]);
                    // Return true
                    return true;
                }
                return false;
            });
        }

        /// <summary>
        /// Delete one item in inventory
        /// </summary>
        private void DeleteItem()
        {
            dialogueRunner.AddFunction("deleteItem", 1, delegate (Yarn.Value[] parameters)
            {
                var inventoryForQuest = getInventoryForQuest(parameters);

                if (inventoryForQuest.isItemExist)
                {
                    var index = inventoryForQuest.index;
                    // Clear one item in this list
                    inventoryForQuest.inventory.itemsCasesInventory[index].RemoveAt(index);
                    // Get button
                    var btn = FindObjectOfType<Inventaire>()._inventoryCasesRight[index];

                    var itemCount = inventoryForQuest.inventory.itemsCasesInventory[index].Count;
                    // If we have no more items, we hide this items in inventory
                    if (itemCount == 0)
                    {
                        FindObjectOfType<Inventaire>().HideCaseInventory(btn);
                    }
                    // Else we update text in this button
                    else
                    {
                        FindObjectOfType<Inventaire>().UpdateCaseInventory(btn, itemCount);
                    }
                    return true;
                }
                return false;
            });
        }

        /// <summary>
        /// Add one item in inventory
        /// </summary>
        private void AddItem()
        {
            dialogueRunner.AddFunction("addItem", 1, delegate (Yarn.Value[] parameters)
            {
                var inventoryForQuest = getInventoryForQuest(parameters);

                // Item exist in inventory
                if (inventoryForQuest.isItemExist)
                {
                    var index = inventoryForQuest.index;
                    // We add the same element of this item
                    var item = inventoryForQuest.inventory.itemsCasesInventory[index][0];
                    inventoryForQuest.inventory.itemsCasesInventory[index].Add(item);

                    // Get button
                    var btn = FindObjectOfType<Inventaire>()._inventoryCasesRight[index];
                    var itemCount = inventoryForQuest.inventory.itemsCasesInventory[index].Count;

                    // Update ui interface
                    FindObjectOfType<Inventaire>().UpdateCaseInventory(btn, itemCount);

                    return true;
                }

                /* Item doesn't exist in inventory */
                var itemName = parameters[0].AsString;
                IItem itemToAdd = null;
                switch (itemName)
                {
                    case "Herbe":
                        itemToAdd = new Grass();
                        break;
                    case "Potion de soin":
                        itemToAdd = new RedPotion();
                        break;
                    case "Pièce de 1":
                        itemToAdd = new Coin1();
                        break;
                    case "Pièce de 10":
                        itemToAdd = new Coin10();
                        break;
                    case "Pièce de 5":
                        itemToAdd = new Coin5();
                        break;
                    case "Moitier de bouclier":
                        itemToAdd = new HalfShield();
                        break;
                    case "Potion de bouclier":
                        itemToAdd = new PotionShield();
                        break;
                    case "Réceptacle rouge":
                        itemToAdd = new RedHeart();
                        break;
                    case "Bouclier":
                        itemToAdd = new Shield();
                        break;
                    case "Durandale":
                        itemToAdd = new Sword();
                        break;
                    default:
                        break;
                }
                // We detect witch type of item and add this item in the list
                FindObjectOfType<Inventaire>().DetectTypeItem(itemToAdd);
                
                return false;
            });
        }

        /// <summary>
        /// Get inventory. We need get this inventory to use this in custom Yarn method 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private InventoryForQuest getInventoryForQuest(Yarn.Value[] parameters)
        {
            var inventoryForQuest = new InventoryForQuest();

            // Get the value of all item in inventory
            inventoryForQuest.inventory = FindObjectOfType<Inventaire>().GetInventaireDataFromInventaire();

            // parameters[0] must be the same name of the item element in inventory (value set in RedPotion.cs for example)
            var itemName = parameters[0];

            for (int i = 0; i < inventoryForQuest.inventory.itemsCasesInventory.Count; i++)
            {
                foreach (var item in inventoryForQuest.inventory.itemsCasesInventory[i])
                {
                    if (item.Name.Contains(itemName.AsString))
                    {
                        inventoryForQuest.isItemExist = true;
                        inventoryForQuest.index = i;
                        return inventoryForQuest;
                    }
                }
            }

            inventoryForQuest.isItemExist = false;
            return inventoryForQuest;
        }
    }
}
