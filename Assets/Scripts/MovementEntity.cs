﻿using System;
using UnityEngine;

[Serializable]
public class MovementEntity
{
    private GameObject _parent;
    public Transform[] waypoints;
    public Transform target;
    public int waypointIndex = 0;
    public float _moveSpeed;
    private DetectPlayer _detect;
    private SpriteRenderer _sprite;
    private bool isMovingLeft;
    private bool isMovingRight;

    public void Initiate(GameObject parent, float moveSpeed, DetectPlayer detect,SpriteRenderer sprite)
    {
        target = waypoints[0];
        _parent = parent;
        _moveSpeed = moveSpeed;
        _detect = detect;
        _sprite = sprite;
    }

    public void Move()
    {
        if (_detect._isRange)
        {
            Vector3 direction = _detect._player.transform.position - _parent.transform.position;
            direction.y = 0;// Locks the y-axis movement when the enemy is attacking
            _parent.transform.Translate(direction.normalized * _moveSpeed * Time.deltaTime, Space.World);

            if (direction.x < 0) // If moving to the left
            {
                _sprite.transform.eulerAngles = new Vector3(0, 0, 0);
            }
            else if (direction.x > 0)
            {
                _sprite.transform.eulerAngles = new Vector3(0, 180, 0);
            }
        }

        else
        {
            Vector3 direction = target.position - _parent.transform.position;
            _parent.transform.Translate(direction.normalized * _moveSpeed * Time.deltaTime, Space.World);

            if (direction.x < 0)
            {
                isMovingLeft = true;
                isMovingRight = false;
                _sprite.transform.eulerAngles = new Vector3(0, 0, 0);
            }
            else if (direction.x > 0)
            {
                isMovingLeft = false;
                isMovingRight = true;
                _sprite.transform.eulerAngles = new Vector3(0, 180, 0);
            }

            if (Vector3.Distance(_parent.transform.position, target.position) < 0.3f && !_detect._isRange)
            {
                waypointIndex = (waypointIndex + 1) % waypoints.Length;
                target = waypoints[waypointIndex];

            }
        }
    }
}
