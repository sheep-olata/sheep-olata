﻿using UnityEngine;

/// <summary>
/// cet interface permet de donner les directives a suivre sur le comportement des objects du jeux
/// </summary>
public interface IItem 
{
    string Type { get;} // ils auront tous un type d'objet comme le type armure ou alors weapon
    bool Pick { get; } // pour savoir si ils sont ramassable
    string Name { get; }
    Sprite Sprite { get; }
    /// <summary>
    /// Méthode qui permet de prendre les items
    /// </summary>
    void PickItem(); // Méthode qui permet de gérer la récuperation de l'item avec le joueur
    /// <summary>
    /// Méthode qui permet d'utiliser les items
    /// </summary>
    void UseItem(); // Méthode pour utiliser de manière générique les items

}
