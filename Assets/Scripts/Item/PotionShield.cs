﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PotionShield : Perks
{
    private int valueShield;
    void Start()
    {
        SetItem();
    }

    public override void SetItem()
    {
        Type = "Potion";
        Name = "Potion de bouclier";
        Pick = true;
        valueShield = 20;
        Sprite = ImagesItems.instance.sprites[2];
    }
    public override void UseItem()
    {
        Debug.Log($"Utilisation de la potion de bouclier");
        Player.instance.CurrentShieldPoint += valueShield;
        Player.instance.shieldBar.UpdateShield(Player.instance.CurrentShieldPoint);
    }

}
