﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Helmet : Head
{
    void Start()
    {
        SetHelmet();
    }

    public override void SetHelmet()
    {
        value = 10;
        Type = "Head";
        Pick = true;
        shield = 5;
        Name = "Casque a la noix de coco";
        Sprite = ImagesItems.instance.sprites[8];
    }

    public override void UseItem()
    {
        Debug.Log($"je m'équipe du casque {this.Name}");
        Player.instance.ShieldPoint += shield;
        Player.instance.shieldBar.SetShieldBar(Player.instance.CurrentShieldPoint, Player.instance.ShieldPoint);
    }

    public override void UnequipItem()
    {
        Debug.Log($"j'enleve le casque {this.Name}");

        if (Player.instance.CurrentShieldPoint - shield < 0) // If the shield will drop to negative after removing the armor, set it to 0
        {
            Player.instance.CurrentShieldPoint = 0;
        }
        else // Else reduce the current shield by the shield amount removed (lost from armor)
        {
            Player.instance.CurrentShieldPoint -= shield;
        }

        Player.instance.ShieldPoint -= shield;
        Player.instance.shieldBar.SetShieldBar(Player.instance.CurrentShieldPoint, Player.instance.ShieldPoint);
    }
}
