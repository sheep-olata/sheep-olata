﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : Weapon
{

    private void Start()
    {
        SetItem();
    }

    public override void SetItem()
    {
        value = 20;
        Type = "Sword";
        Pick = true;
        damage = 10;
        Name = "Durandale";
        Sprite = ImagesItems.instance.sprites[7];
    }

    public override void UseItem()
    {
        Debug.Log($"je m'équipe de l'épée {this.Name}");
        Player.instance.DamagePoint += damage;
    }

    public override void UnequipItem()
    {
        Debug.Log($"j'enleve l'épée {this.Name}");
        Player.instance.DamagePoint -= damage;
    }
}
