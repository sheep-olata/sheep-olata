﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Armor : MonoBehaviour, IItem
{
    public string Type { get; protected set; }
    public string Name { get; protected set; }
    public bool Pick { get; protected set; }
    public int value { get; set; }
    public int shield { get; protected set; }
    public Sprite Sprite { get; protected set; }
    public abstract void SetArmor();
    public abstract void UseItem();
    public abstract void UnequipItem();

    public void PickItem()
    {
        if (this.Pick)
        {
            //ActionOnItem();
            Inventaire.instance.FillInventory(this);
            Destroy(this.gameObject);
            Debug.Log("je peux détruire ca !");
        }
        else
        {
            Debug.Log("je ne peux pas détruire ca !");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            PickItem();
        }
    }

}
