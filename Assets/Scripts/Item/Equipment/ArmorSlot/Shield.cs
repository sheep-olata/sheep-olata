﻿using UnityEngine;
public class Shield : Armor
{
    void Start()
    {
        SetArmor();
    }

    public override void SetArmor()
    {
        value = 10;
        Type = "Armor";
        Pick = true;
        shield = 20;
        Name = "Bouclier Nul";
        Sprite = ImagesItems.instance.sprites[3];
    }

    public override void UseItem()
    {
        Debug.Log($"je m'équipe du bouclier {this.Name}");
        Player.instance.ShieldPoint += shield;
        Player.instance.shieldBar.SetShieldBar(Player.instance.CurrentShieldPoint, Player.instance.ShieldPoint);
    }

    public override void UnequipItem()
    {
        Debug.Log($"j'enleve le bouclier {this.Name}");

        if (Player.instance.CurrentShieldPoint == Player.instance.ShieldPoint) // If the shield will drop to negative after removing the armor, set it to 0
        {
            Player.instance.CurrentShieldPoint -= shield;
        }
        
        Player.instance.ShieldPoint -= shield;
        Player.instance.shieldBar.SetShieldBar(Player.instance.CurrentShieldPoint, Player.instance.ShieldPoint);
    }
}
