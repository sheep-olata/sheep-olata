﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Plant : MonoBehaviour, IItem
{
    public string Type { get; protected set; }
    public string Name { get; protected set; }

    public bool Pick { get; protected set; }

    public int hpGain { get; protected set; }

    public Sprite Sprite { get; protected set; }

    public abstract void SetPlant();
    public abstract void UseItem();
    public void PickItem()
    {
        if (this.Pick)
        {
            Inventaire.instance.FillInventory(this);
            Destroy(this.gameObject);
        }
        else
        {
            Debug.Log("je ne détruit pas l'objet");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            PickItem();
        }
    }


}
