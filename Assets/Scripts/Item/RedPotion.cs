﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedPotion : Perks
{
    private int _valueHeal;
    void Start()
    {
        SetItem();
    }

    public override void SetItem()
    {
        _valueHeal = 20;
        Pick = true;
        Name = "Potion de soin";
        Type = "Perks";
        Sprite = ImagesItems.instance.sprites[0];
    }

    public override void UseItem()
    {
        Debug.Log($"Utilisattionde la potion de vie sur le joueur");
        Player.instance.CurrentHealthPoint += _valueHeal;
        Player.instance.healthBar.UdateHealthBar(Player.instance.CurrentHealthPoint);
    }
}
