﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImagesItems : MonoBehaviour
{
    public Sprite[] sprites;
    public static ImagesItems instance;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.Log("il y' a plus d'une instance de ImagesItems dnas la scene");
        }
        instance = this;
    }
}
