﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Perks : MonoBehaviour, IItem
{
    public string Type { get; protected set; }
    public string Name { get; protected set; } 
    public bool Pick { get; protected set; }

    public Sprite Sprite { get; protected set; }
    /// <summary>
    /// permet de ramasser l'objet en fonction de son status "Pick"
    /// </summary>
    public void PickItem()
    {
        if (Pick)
        {
            Inventaire.instance.FillInventory(this);
            Destroy(this.gameObject);
        }
    }
    /// <summary>
    /// action sur l'item
    /// </summary>
    /// <summary>
    /// Ajoute des caractérique a l'objet ramasser
    /// </summary>
    public abstract void SetItem();

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            PickItem();
        }
    }

    public abstract void UseItem();
}
