﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin10 : Coins
{
    void Start()
    {
        SetItem();
    }
    protected override void SetItem()
    {
        Value = 10;
        Type = "coin";
        Name = "Pièce de 10";
        Pick = true;
        Sprite = ImagesItems.instance.sprites[4];

    }

    public override void UseItem()
    {
        throw new System.NotImplementedException();
    }
}
