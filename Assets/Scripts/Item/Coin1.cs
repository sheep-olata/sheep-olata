﻿
/// <summary>
/// Class fille de la class abstraite Coins elle extend de cette dernière
/// </summary>
public class Coin1 : Coins
{
    // Start is called before the first frame update
    void Start()
    {
        SetItem(); // on met a jour l'objet
    }
    /// <summary>
    /// On réécri la fonction SetItem de la class abstraite
    /// </summary>
    protected override void SetItem()
    {
        Value = 1; // on met a jour la valeur du coin
        Type = "coin"; // on met a jour le type d'item
        Name = "Pièce de 1";
        Pick = true; // on active le ramassage
        Sprite = ImagesItems.instance.sprites[4];

    }

    public override void UseItem()
    {
        throw new System.NotImplementedException();
    }
}
