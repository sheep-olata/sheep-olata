﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedHeart : Perks
{
    private int _valueRedHeart;
    void Start()
    {
        SetItem();
    }

    public override void SetItem()
    {
        _valueRedHeart = 10;
        Pick = true;
        Name = "Réceptacle rouge";
        Type = "Perks";
        Sprite = ImagesItems.instance.sprites[1];
    }

    public override void UseItem()
    {
        Debug.Log($"Augmentation de la barre de vie du joueur");
        //Player.instance._maxLifePoint += _valueRedHeart;
        //HealtBar.instance.SetHealthBar();
    }
}
