﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grass : Plant
{
    private void Start()
    {
        SetPlant();
    }

    public override void SetPlant()
    {
        Pick = true;
        hpGain = 5;
        Type = "plant";
        Name = "Herbe";
        Sprite = ImagesItems.instance.sprites[5];
    }
    public override void UseItem()
    {
        Debug.Log($"Utilisationde l'herbe sur le joueur");
        Player.instance.CurrentHealthPoint += hpGain;
        Player.instance.healthBar.UdateHealthBar(Player.instance.CurrentHealthPoint);
        Player.instance.SetHasEatGrass(true);
    }
}
