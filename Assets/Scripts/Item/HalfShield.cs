﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HalfShield : Armor
{

    void Start()
    {
        SetArmor();
    }

    public override void SetArmor()
    {
        Type = "Armor";
        Name = "Moitier de bouclier";
        value = 5;
        Pick = true;
        Sprite = ImagesItems.instance.sprites[6];
    }

    public override void UseItem()
    {
        Debug.Log($"Le joueur regagne ${this.value} point de bouclier");
    }

    public override void UnequipItem()
    {
    }
}
