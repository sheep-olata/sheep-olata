﻿using UnityEngine;
using UnityEngine.UI;
using System;
/// <summary>
/// Une class qui sera redéfini plus tard pour varier les comportement de ce même type d'objet
/// </summary>
[Serializable]
public abstract class Coins : MonoBehaviour,IItem
{
    public int Value { get; protected set; } // attribut qui permet de donner une valeur "monetaire" au coin associer
    public bool Pick { get; protected set; } // on retrouve une valeur de l'interface
    public string Name { get; protected set; }

    public string Type { get; protected set; } // on retrouve une valeur de l'interface
    
    public Sprite Sprite { get; protected set; }
    protected string _typeCoin { get; set; } // attribu qui permet de savoir quel type de coin (pièce) c'est or,cuivre,argent

    protected abstract void SetItem(); // méthode a redéfinir dans la class fille

    public string TypeCoin // cela permet de modifier et récuperer la valeur _typeCoin
    {
        get { return _typeCoin; }
        set { _typeCoin = value; }
    }
    /// <summary>
    /// Méthode donnée par l'interface elle défini le comportement de l'objet lors de son ramassage 
    /// </summary>
    public void PickItem() 
    {
        if (this.Pick) // si l'attribu Pick est true alors on execute le code suivant
        {
            Inventaire.instance.refillAccount(this.Value); // utilise la méthode refillAccount de l'inventaire pour mettre a jour les info de la bourse du joueur
            Debug.Log($"Le joueur reçoit {Value} flower/s dans sa poche");
            Destroy(this.gameObject); // on détruit l'objet pour ne pas reçevoir les actions en boucle 
            Debug.Log("je détrui l'objet");
        }
        else
        {
            Debug.Log("je ne détrui pas l'objet");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player")) // si la collision est bien le joueur alors on execute le code suivant
        {
            PickItem(); // on lance la méthode PickItem
        }
    }

    public abstract void UseItem();
}
