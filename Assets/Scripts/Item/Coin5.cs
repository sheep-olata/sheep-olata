﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin5 : Coins
{
    // Start is called before the first frame update
    void Start()
    {
        SetItem();
    }
    protected override void SetItem()
    {
        Value = 5;
        Type = "coin";
        Pick = true;
        Name = "Pièce de 5";
        Sprite = ImagesItems.instance.sprites[4];

    }

    public override void UseItem()
    {
        throw new System.NotImplementedException();
    }

}
