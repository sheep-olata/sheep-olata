﻿using System;
using UnityEngine;

[Serializable]
public class EnemyHealthBarMove
{
    public HealthBar healthBar = new HealthBar();
    public GameObject slider;
    public Vector3 offset;
    public void Move(GameObject target)
    {
        slider.transform.position = Camera.main.WorldToScreenPoint(target.transform.position + offset);
    }
}
