﻿using System;
using UnityEngine;
using UnityEngine.UI;

[Serializable]

public class ExpBar 
{
    public Slider slider;
    public Text percentIndicator;
    public Gradient gradient;
    public Image fill;

    public void SetExpBar(int curExp, int expMax)
    {
        percentIndicator.text = $"0%";
        slider.value = curExp;
        slider.maxValue = expMax;
        fill.color = gradient.Evaluate(1f);
    }

    public void UpDateEXPBar(int curExp)
    {
        percentIndicator.text = $"{GetPercent(curExp)}%";
        slider.value = curExp;
        fill.color = gradient.Evaluate(slider.normalizedValue);
    }
    private double GetPercent(int curExp)
    {
        int max = Player.instance.HealtPoint;
        double rest = ((double)curExp/max);
        double result = rest * 100;
        return result;
    }
}
