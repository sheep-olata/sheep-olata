﻿using UnityEngine;
using UnityEngine.UI;
using System;

[Serializable]

public class HealthBar
{
    public Slider slider;
    public Gradient gradient;
    public Image fill;

    public void SetHeathBar(int currentHeal,int health)
    {
        slider.maxValue = health;
        slider.value = currentHeal;
        fill.color = gradient.Evaluate(1f);
    }

    public void UdateHealthBar(int currentHealth)
    {
        slider.value = currentHealth;
        fill.color = gradient.Evaluate(slider.normalizedValue);
    }
}
