﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryMenu : MonoBehaviour
{
    private void Update()
    {
        if (GameManager.instance.m_controller.InventoryInputPressed() && GameManager.instance.m_menuManager.GetLastMenuOpen() == "")
        {
            OpenInventoryUI();
        }else if(GameManager.instance.m_controller.InventoryInputPressed() && GameManager.instance.m_menuManager.GetLastMenuOpen() == "Inventory")
        {
            CloseInventory();
        }
    }

    public void OpenInventoryUI()
    {
        GameManager.instance.GetMenuManager().OpenMenuByName("Inventory");
        GameManager.instance.SetPause(true);
    }

    public void CloseInventory(bool pauseActivate = false)
    {
        GameManager.instance.GetMenuManager().CloseMenu();
        GameManager.instance.SetPause(pauseActivate);
    }
}
