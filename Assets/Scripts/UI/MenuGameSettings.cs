﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MenuGameSettings : MonoBehaviour
{
    private Resolution[] m_resolutions;
    public GameObject m_groupSettings;
    [SerializeField] private TMP_Dropdown m_dDResolutions;
    [SerializeField] private Toggle m_toggleFullScreen;
    [SerializeField] private Slider m_sliderSpeedTest;
    private Resolution m_currentResolution;

    private int m_frameRate = 60;
    private void Awake()
    {
        m_resolutions = Screen.resolutions;
        m_currentResolution = Screen.currentResolution;
        for (int i = 0; i < m_groupSettings.transform.childCount; i++)
        {
            GameObject child = m_groupSettings.transform.GetChild(i).gameObject;
            if (child.name.Contains("Resolutions") && child.name.Contains("DD"))
            {
                m_dDResolutions = child.GetComponent<TMP_Dropdown>();
            }else if (child.name.Contains("FullScreen") && child.name.Contains("Toggle"))
            {
                m_toggleFullScreen = child.GetComponent<Toggle>();
            }
            else if (child.name.Contains("SpeedText") && child.name.Contains("Slider"))
            {
                m_sliderSpeedTest = child.GetComponent<Slider>();
            }
        }
        InitSubMenu();
    }

    /// <summary>
    /// method to initiate the dropDown which contains all the resolutions of the target screen
    /// </summary>
    private void SetDropDownResolution()
    {
        List<string> dDOptions = new List<string>();
        m_dDResolutions.ClearOptions();
        for (int i = 0; i < m_resolutions.Length; i++)
        {
            dDOptions.Add($"{m_resolutions[i].width}x{m_resolutions[i].height}");
        }
        m_dDResolutions.AddOptions(dDOptions);
        for (int i = 0; i < m_dDResolutions.options.Count; i++)
        {
            if (m_dDResolutions.options[i].text.Contains($"{m_currentResolution.width}x{m_currentResolution.height}"))
            {
                m_dDResolutions.value = i;
            }
        }
    }

    private void InitSubMenu()
    {
        SetDropDownResolution();
        m_toggleFullScreen.isOn = Screen.fullScreen;
    }

    public void SetFullScreen(bool fullScreen)
    {
        Screen.fullScreen = fullScreen;
    }

    /// <summary>
    /// Method set screen resolution of target screen
    /// </summary>
    public void SetResolution()
    {
        string[] resolution = m_dDResolutions.options[m_dDResolutions.value].text.Split('x');
        int width = int.Parse(resolution[0]);
        int height = int.Parse(resolution[1]);
        Screen.SetResolution(width, height, m_toggleFullScreen.isOn,m_frameRate);
    }

}
