﻿using UnityEngine;
using UnityEngine.UI;
using System;

[Serializable]

public class ShieldBar
{
    public Slider slider;

    public Gradient gradient;
    public Image fill;

    public void SetShieldBar(int currentShiedl, int maxShield)
    {
        slider.maxValue = maxShield;
        slider.value = currentShiedl;

        fill.color = gradient.Evaluate(1f);
    }

    public void UpdateShield(int currentShield)
    {
        slider.value = currentShield;

        fill.color = gradient.Evaluate(slider.normalizedValue);
    }

}
