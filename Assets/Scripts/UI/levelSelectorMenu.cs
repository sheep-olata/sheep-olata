﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class levelSelectorMenu : MonoBehaviour
{
    public void loadLevel(int level) 
    {
        UnityEngine.Debug.Log("Level " + level);
        SceneManager.LoadScene("Level " + level);

    }
}
