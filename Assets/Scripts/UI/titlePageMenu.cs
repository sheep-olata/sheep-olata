﻿using System.Net.Mime;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class titlePageMenu : MonoBehaviour
{
    // Get Option Canvas
    [SerializeField] private GameObject menuCanvas = null;
    [SerializeField] private GameObject optionCanvas = null;
    [SerializeField] private GameObject newAdventureCanvas = null;
     
    // Get Button
    [SerializeField] private GameObject button = null;

    // Get Animation from Animator
    private Animator anim;

    // Get Audio files
    public AudioSource audioSource;
    public AudioClip sound;
 

    // Initialize components
    void Start()
    {
        anim = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
    }

    // Is called when mouse is over
    private void OnMouseEnter()
    {
        // Reset Audio source before playing new sound
        audioSource.Stop();
        audioSource.PlayOneShot(sound);

        // Change the transition status to launch the animation from animator
        anim.SetBool("doAnimation", true);
    }
    
    // Is called when mouse is out
    private void OnMouseExit()
    {
        // Reset Audio source before playing new sound
        audioSource.Stop();

        // Change the transition status to launch the animation from animator
        anim.SetBool("doAnimation", false);
    }

    // Is called when mouse is pressed
    private void OnMouseDown()
    {
        switch(button.name) 
        {
            // Try if button pressed equals a defined button name
            case "TMNew":
                // Display New Adventure Menu
                menuCanvas.SetActive(false);
                newAdventureCanvas.SetActive(true);
                break;

            case "TMContinue":
                FindObjectOfType<EventManager>().loadingGame = true;
                SceneManager.LoadScene(SaveManager.GetLevelToLoad());

                break;

            case "TMSettings":
                // Display Settings Menu
                menuCanvas.SetActive(false);
                optionCanvas.SetActive(true);
                break;

            case "TMQuit":
                // Quit Application
                // Precompilation test the environment before quitting to prevent the loss of work
                // Allow to quit when we're on unity and .exe
                #if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
                #else
                Application.Quit();
                #endif
                break;

            case "TMReturnMenu":
                // Display Title Menu (disable other canvas)
                optionCanvas.SetActive(false);
                newAdventureCanvas.SetActive(false);
                menuCanvas.SetActive(true);
                break;

            case "TMMore":
                // Open Website (in the future Sheep'Olata Website)
                Application.OpenURL("https://unity.com");
                break;
                
            default:
                // Default Return Debug Message
                UnityEngine.Debug.Log("Unity SO:00001 Unexpected selection in TitlePage menu");
                break;
        }
     } 
}