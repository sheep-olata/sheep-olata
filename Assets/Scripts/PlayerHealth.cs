using System.Collections;
using UnityEngine;


public class PlayerHealth : MonoBehaviour
{
    public int maxHealth = 100;
    public int currentHealth;

    public int maxShield = 100;
    public int currentShield;

    public bool isInvincible = false;

    public SpriteRenderer graphics;
    public HealthBar healthBar;
    public ShieldBar shieldBar;

    public static PlayerHealth instance;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("Il y a plus d'une instance de PlayerHealth dans la scène");
            return;
        }
        instance = this;
    }

    void Start()
    {

    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            TakeDamage(20);
        }
    }

    /*public void HealPlayer(int amount)
    {
        if((currentHealth + amount) > maxHealth)
        {
            currentHealth = maxHealth;
        }
        else
        {
            currentHealth += amount;
        }

        healthBar.SetHealth(currentHealth);
    }*/

    public void TakeDamage(int damage)
    {
        if (!isInvincible && currentShield == 0)
        {
            Player.instance.TakeDamage(damage);
            isInvincible = true;
        }

        else if (!isInvincible)
        {
            Player.instance.TakeDamage(damage);
            isInvincible = true;
        }
    }

}