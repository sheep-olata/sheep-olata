﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public GameObject[] enemyArray; // List for the animals
    private int index;

    // Start is called before the first frame update
    void Start()
    {
        // Calls Spawn for the initial start of the game
        Spawn();

        // Invokes repeating loop to call Spawn every second
        InvokeRepeating("Spawn", 1f, 1f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // Function to spawn enemies
    void Spawn()
    {
        // Sets a random animal from the list
        int animalIndex = Random.Range(0, 1);

        if (Player.instance.isTeleporting)
        {
            var clones = GameObject.FindGameObjectsWithTag("enemyClone");

            foreach (var clone in clones)
            {
                Destroy(clone);
            }

            // Initializes the enemy
            foreach (GameObject enemyUnit in enemyArray)
            {
                index = enemyUnit.GetComponent<GetSpawn>().enemyIndex;
                GameObject enemySpawned = Instantiate(enemyArray[index], enemyArray[index].transform.position, enemyArray[index].transform.rotation);
                enemySpawned.SetActive(true);
                enemySpawned.tag = "enemyClone";
            }
        }
    }
}
