﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[System.Serializable]
public struct InventaireData//serializable struct containing useful data from Inventaire object for save/load
{
    public int currentAccount;
    public int account;
    public int currentIndex;
    public List<IItem> items;
    public List<List<IItem>> itemsCasesInventory;
    public int countItems;
    public List<List<IItem>> stuffs;

}
public class Inventaire : MonoBehaviour
{
    public static Inventaire instance;
    public int currentAccount;
    public int account;
    public Text textAccount;
    public int currentIndex;
    public GameObject btnItem;
    public Image _btnSprite;
    public List<IItem> items = new List<IItem>();
    private List<List<IItem>> itemsCasesInventory = new List<List<IItem>>();
    public int countItems;
    public GameObject uiInventory;
    public List<GameObject> _inventoryCasesRight = new List<GameObject>();
    public List<GameObject> _invetoryCaseLeft = new List<GameObject>();
    private List<List<IItem>> stuffs = new List<List<IItem>>();
    private bool isOpen = false;

    /// <summary>
    /// Mise en place du singleton
    /// </summary>
    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("Il y'a plus d'une instace de Inventaire dans cette scene");
            return;
        }
        account = 0;
        currentAccount = 0;
        textAccount.text = currentAccount.ToString();
        instance = this;
        AssociateItemsAndButtons();
    }

    private void Update()
    {
        //if (Input.GetKeyDown(KeyCode.I) || Input.GetKeyDown(KeyCode.Escape) && isOpen)
        //{
        //    InteractUIIventory();
        //}
    }

    #region save/load
    public void LoadFromInventaireData(InventaireData data)
    {
        currentAccount = data.currentAccount;
        account = data.account;
        currentIndex = data.currentIndex;
        items = data.items;
        itemsCasesInventory = data.itemsCasesInventory;
        countItems = data.countItems;
        stuffs = data.stuffs;
    }


    public InventaireData GetInventaireDataFromInventaire()
    {
        InventaireData data = new InventaireData();
        data.currentAccount = currentAccount;
        data.account = account;
        data.currentIndex = currentIndex;
        data.items = items;
        data.itemsCasesInventory = itemsCasesInventory;
        data.countItems = countItems;
        data.stuffs = stuffs;
        return data;
    }
    #endregion

    /// <summary>
    /// Méthode qui permet d'initialiser l'inventaire en mettant l'ensemble des boutons
    /// avec une methode regroupant une list et un bouton(case) de l'inventaire
    /// </summary>
    private void InitiateInventory()
    {
        for (int i = 0; i < _inventoryCasesRight.Count; i++)
        {
            GameObject btn = _inventoryCasesRight[i];
            List<IItem> lst = itemsCasesInventory[i];
            btn.GetComponent<Button>().onClick.AddListener(() => ActionOnItem(btn,lst));
        }
        for (int y = 0; y < _invetoryCaseLeft.Count; y++)
        {
            GameObject btn = _invetoryCaseLeft[y];
            List<IItem> stuff = stuffs[y];
            btn.GetComponent<Button>().onClick.AddListener(() => ActionOnStuff(btn,stuff));
        }
    }

    /// <summary>
    /// Methode qui permet d'associer les boutons au list 
    /// </summary>
    private void AssociateItemsAndButtons()
    {
        InititateItemsInInventory();// ajoute autant de list dans le tableau des lists que de boutons dans l'inventaire
        InitiateInventory(); // associe les list avec les boutons de l'inventaire
        foreach (GameObject btnRight in _inventoryCasesRight)
        {
            GameObject imgBtn = btnRight.transform.Find("Image").gameObject;
            GameObject txtBtn = btnRight.transform.Find("CounterItem").gameObject;
            MakeHideCaseInvetory(imgBtn.GetComponent<Image>(), txtBtn.GetComponent<Text>());
        }
        foreach(GameObject btnLetf in _invetoryCaseLeft)
        {
            GameObject imgBtn = btnLetf.transform.Find("Image").gameObject;
            GameObject txtBtn = btnLetf.transform.Find("CounterItem").gameObject;
            MakeHideCaseInvetory(imgBtn.GetComponent<Image>(), txtBtn.GetComponent<Text>());
        }
    }

    /// <summary>
    /// Initialise les listes pour les ajouter qui seront ajouter aux différent boutons
    /// </summary>
    private void InititateItemsInInventory()
    {
        for (int i = 0; i < _inventoryCasesRight.Count; i++)
        {
            itemsCasesInventory.Add(new List<IItem>());
        }
        for (int y = 0; y < _invetoryCaseLeft.Count; y++)
        {
            stuffs.Add(new List<IItem>());
        }
        //Debug.Log($"Il y'a {itemsCasesInventory.Count} liste d'item pour {_inventoryCasesRight.Count} case/btn dans l'inventaire");
    }
    /// <summary>
    /// méhode qui permet de consommer l'objet dans le menu rapide
    /// </summary>
    public void ConsumeItem()
    {
        IItem currentItem = items[currentIndex];
        if (currentItem != null)
        {
            if (currentItem is Plant)
            {
                Plant plant = currentItem as Plant;
                if (plant is Grass)
                {
                    Grass grass = plant as Grass;
                    grass.UseItem();
                }
            }
            else
            {
                Debug.Log("l'objet n'est pas une Plant");
            }
            Debug.Log("l'objet a été consummé");
            items.Remove(currentItem);
        }
        else
        {
            Debug.Log("Il n'y a pas d'objet dans l'inventaire");
        }
        GetPrevItem();
        countItems = items.Count;
        //CheckIventory();
    }
    /// <summary>
    /// méthode qui permet de récupérer l'objet suivant
    /// </summary>
    public void GetNextItem()
    {
        currentIndex++;
        if(currentIndex > items.Count - 1)
        {
            currentIndex = 0;
        }
        else
        {
        }
        ChangeImage();
    }
    /// <summary>
    /// méthode qui récupérer l'objet précédent
    /// </summary>
    public void GetPrevItem()
    {
        currentIndex--;
        if (currentIndex < 0)
        {
            if(items.Count == 0)
            {
                currentIndex = 0;
            }
            else
            {
                currentIndex = items.Count - 1;
            }
        }
        Debug.Log($"item name's: {items[currentIndex].Name} and sprite's name: {items[currentIndex].Sprite.name}");
        ChangeImage();
    }
    
    /*public void CheckIventory()
    {
        if(items.Count == 0)
        {
            btnItem.GetComponent<Button>().interactable = false;
        }
        else
        {
            btnItem.GetComponent<Button>().interactable = true;
        }
    }*/


    /// <summary>
    /// Méthode qui permet d'ajouter des pièces au compte du joueur
    /// </summary>
    /// <param name="coinValue"></param>
    public void refillAccount(int coinValue)
    {
        currentAccount += coinValue; // on additionne a la valeur de actuel la valeur du coin
        textAccount.text = currentAccount.ToString(); // on actualise l'affichage
    }


    /// <summary>
    /// Méthode qui permet d'ajouter a l'inventaire les objets qui sont instancier en tant que IItem
    /// </summary>
    /// <param name="item"></param>
    public void FillInventory(IItem item)
    {
        DetectTypeItem(item);
        object test = item as object;
        //Debug.Log($"le type de l'objet est: {test.GetType()}");
    }

    /// <summary>
    /// Permet de changer l'image du bouton d'acces rapide
    /// </summary>
    private void ChangeImage()
    {
        _btnSprite.sprite = items[currentIndex].Sprite;
    }

    private void ChangeImageOnInventory(int btnItem, IItem item)
    {
        GameObject btn = _inventoryCasesRight[btnItem];
        GameObject imgBtn = btn.transform.Find("Image").gameObject;
        GameObject txtBtn = btn.transform.Find("CounterItem").gameObject;
        Image btnSprite = imgBtn.GetComponent<Image>();
        MakeShowCaseInvetory(btnSprite,txtBtn.GetComponent<Text>());
        btnSprite.sprite = item.Sprite;
    }

    /// <summary>
    /// Méthode qui vérifie si la list en paramètre est vide
    /// </summary>
    /// <param name="list"></param>
    /// <returns> si la liste a au moins 1 item alors elle revoie true si non elle revoie false</returns>
    private bool CheckItemTypeOnList(List<IItem> list)
    {
        if(list.Count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    /// <summary>
    /// Méthode qui permet de savoir quel type d'objet le joueur ramasse et le stocker dans une list (vide de préfénce) 
    /// </summary>
    /// <param name="item"></param>
    public void DetectTypeItem(IItem item)
    {
        if (item is Plant)
        {
            Plant plant = item as Plant;
            if (plant is Grass)
            {
                Grass grass = new Grass();
                grass.SetPlant();
                if (CompareItem(grass))
                {
                    Debug.Log("l'ojet de type herbe a été ajouter dans une list deja rempli");
                }
                else if (FindEmptyList(grass))
                {
                    Debug.Log("l'ojet de type herbe a été ajouter dans une list vide");
                }
                else
                {
                    Debug.LogError("L'objet n'a pas été ajouter");
                }
                Debug.Log("c'est une plante");
            }
        }
        else if (item is Perks)
        {
            Perks perk = item as Perks;
            if (perk is RedPotion)
            {
                RedPotion redPotion = perk as RedPotion;
                redPotion.SetItem();
                if (CompareItem(redPotion))
                {
                    Debug.Log("l'ojet de type potion rouge a été ajouter dans une list deja rempli");
                }
                else if (FindEmptyList(redPotion))
                {
                    Debug.Log("l'ojet de type potion rouge a été ajouter dans une list vide");
                }
                else
                {
                    Debug.LogError("L'objet n'a pas été ajouter");
                }
                Debug.Log("c'est une potion");
            }
            else if (perk is PotionShield)
            {
                PotionShield potionShield = perk as PotionShield;
                potionShield.SetItem();
                if (CompareItem(potionShield))
                {
                    Debug.Log("l'objet de type potion de bouclier a été ajouter dans une list deja rempli");
                }
                else if (FindEmptyList(potionShield))
                {
                    Debug.Log("l'objet de type potion de bouclier a été ajouter dans une list vide");
                }
                else
                {
                    Debug.LogError("L'objet n'a pas été ajouter");
                }
                Debug.Log("c'est une potion de bouclier");
            }
        }
        else if (item is Armor)
        {
            Armor armor = item as Armor;
            if (armor is Shield)
            {
                Shield shield = armor as Shield;
                shield.SetArmor();
                if (CompareItem(shield))
                {
                    Debug.Log($"l'objet de type armure a été ajouter dans une list deja rempli");
                }
                else if (FindEmptyList(shield))
                {
                    Debug.Log("l'objet de type bouclier a été ajouter dans une list vide");
                }
                else
                {
                    Debug.LogError("L'objet n'a pas été ajouter");
                }
                Debug.Log("c'est une armure");
            }
        }
        else if (item is Weapon)
        {
            Weapon weapon = item as Weapon;
            if (weapon is Sword)
            {
                Sword sword = weapon as Sword;
                sword.SetItem();
                if (CompareItem(sword))
                {
                    Debug.Log($"l'objet de type épée a été ajouter dans une list deja rempli");
                }
                else if (FindEmptyList(sword))
                {
                    Debug.Log("l'objet de type épée a été ajouter dans une list vide");
                }
                else
                {
                    Debug.LogError("L'objet n'a pas été ajouter");
                }
                Debug.Log("c'est un casque");
            }
        }
        else if (item is Head)
        {
            Head head = item as Head;
            if (head is Helmet)
            {
                Helmet helmet = head as Helmet;
                helmet.SetHelmet();
                if (CompareItem(helmet))
                {
                    Debug.Log($"l'objet de type casque a été ajouter dans une list deja rempli");
                }
                else if (FindEmptyList(helmet))
                {
                    Debug.Log("l'objet de type casque a été ajouter dans une list vide");
                }
                else
                {
                    Debug.LogError("L'objet n'a pas été ajouter");
                }
                Debug.Log("c'est une épée");
            }
        }
        else
        {
            Debug.Log("j'ai ramasser une objet mais je ne sais pas ce que c'est :(");
        }
    }
    /// <summary>
    /// Méthode appliquer sur les boutons qui agirons sur les items contenu dans l'inventaire
    /// </summary>
    /// <param name="btn"></param>
    /// <param name="lst"></param>
    private void ActionOnItem(GameObject btn,List<IItem> lst)
    {
        if(!CheckItemTypeOnList(lst)) // si la list est vide on ne fait rien
        {
            Debug.Log($"la liste est vide ! {lst.Count}");
        }
        else // si non on utilise l'item
        {
            //Debug.Log($"la liste n'est pas vide elle contient {lst.Count} items");
            UseItem(lst, btn);
        }
    }
    /// <summary>
    /// Méthode appliquer sur les boutons qui agirons sur les équipements
    /// </summary>
    /// <param name="btn"></param>
    /// <param name="lst"></param>
    private void ActionOnStuff(GameObject btn,List<IItem> lst)
    {
        string nameBtn = btn.name;
        switch (nameBtn)
        {
            case "BtnWeapon":
                if(lst.Count != 0)
                {
                    if (FindEmptyList(lst[0]))
                    {
                        Weapon weapon = (Weapon) lst[0];
                        weapon.UnequipItem();

                        Debug.Log("Arme déséquiper");
                        GameObject imgBtn = btn.transform.Find("Image").gameObject;
                        GameObject txtBtn = btn.transform.Find("CounterItem").gameObject;
                        MakeHideCaseInvetory(imgBtn.GetComponent<Image>(), txtBtn.GetComponent<Text>());
                    }
                    else
                    {
                        Debug.Log("Impossible de stocker, erreur");
                    }
                }
                else
                {
                    Debug.Log("Il y'a rien");
                }
                break;
            case "BtnArmor":
                if(lst.Count != 0)
                {
                    if (FindEmptyList(lst[0]))
                    {
                        Armor armor = (Armor)lst[0];
                        armor.UnequipItem();

                        Debug.Log("Armure déséquiper");
                        GameObject imgBtn = btn.transform.Find("Image").gameObject;
                        GameObject txtBtn = btn.transform.Find("CounterItem").gameObject;
                        MakeHideCaseInvetory(imgBtn.GetComponent<Image>(), txtBtn.GetComponent<Text>());
                    }
                    else
                    {
                        Debug.Log("Impossible de stocker, erreur");
                    }
                }
                else
                {
                    Debug.Log("Il y'a rien");
                }
                break;
            case "BtnHelmet":
                if (lst.Count != 0)
                {
                    if (FindEmptyList(lst[0]))
                    {
                        Head head = (Head)lst[0];
                        head.UnequipItem();

                        Debug.Log("Casque déséquiper");
                        GameObject imgBtn = btn.transform.Find("Image").gameObject;
                        GameObject txtBtn = btn.transform.Find("CounterItem").gameObject;
                        MakeHideCaseInvetory(imgBtn.GetComponent<Image>(), txtBtn.GetComponent<Text>());
                    }
                    else
                    {
                        Debug.Log("Impossible de stocker, erreur");
                    }
                }
                else
                {
                    Debug.Log("Il y'a rien");
                }
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Méthode qui permet d'ouvrir ou de fermer l'interface de l'inventaire
    /// si ouvert il met le jeu qui est en fond en pause et si il est fermer alors le jeu reprend/continu
    /// </summary>
    public void InteractUIIventory()
    {
        if (!isOpen)
        {
            isOpen = true;
            Time.timeScale = 0;
            uiInventory.SetActive(isOpen);
        }
        else
        {
            isOpen = false;
            Time.timeScale = 1;
            uiInventory.SetActive(isOpen);
        }
    }
    /// <summary>
    /// Méthode qui cherche une list vide
    /// </summary>
    /// <param name="objet"></param>
    /// <returns></returns>
    private bool FindEmptyList(object objet)
    {
        int i = 0;
        foreach (List<IItem> lst in itemsCasesInventory)
        {

            if(lst.Count == 0)
            {
                lst.Add((IItem)objet);
                ChangeText(i);
                ChangeImageOnInventory(i,(IItem)objet);
                return true;
            }
            i++;
        }
        return false;
    }
    /// <summary>
    /// Méthode qui compare les items de chaque list non vide et ajoute si elle possède le même type
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    private bool CompareItem(object obj)
    {
        int i = 0;
        foreach (List<IItem> lstItem in itemsCasesInventory)
        {

            if (lstItem.Count > 0)
            {
                var item = lstItem[0];
                if (obj.GetType() == item.GetType())
                {
                    lstItem.Add((IItem)obj);
                    ChangeText(i);
                    return true;
                }
            }
            i++;
        }
        return false;
    }
    /// <summary>
    /// Méthode qui permet de mettre en transparent le fond lorsqu'il y'a aucun objet dans la case de l'inventaire
    /// </summary>
    /// <param name="bgBtn"></param>
    private void MakeHideCaseInvetory(Image bgBtn,Text txt)
    {
        bgBtn.color = new Color(255f, 255f, 255f, 0f);
        txt.color = new Color(255f, 255f, 255f, 0f);
    }
    /// <summary>
    /// Méthode qui permet d'afficher l'image lorsqu'il y'a un objet dans la case de l'inventaire
    /// </summary>
    /// <param name="bgBtn"></param>
    private void MakeShowCaseInvetory(Image bgBtn, Text txt)
    {
        bgBtn.color = new Color(255f, 255f, 255f, 255f);
        txt.color = new Color(255f, 255f, 255f, 255f);
    }
    /// <summary>
    /// Méthode qui modifie le champ de text du bouton
    /// </summary>
    /// <param name="indexBtns"></param>
    private void ChangeText(int indexBtns)
    {
        GameObject btn = _inventoryCasesRight[indexBtns];
        GameObject txtBtn = btn.transform.Find("CounterItem").gameObject;
        txtBtn.GetComponent<Text>().text = $"x{itemsCasesInventory[indexBtns].Count}";
    }
    /// <summary>
    /// Méthode qui est appelée lors de l'action d'un bouton de l'inventaire
    /// </summary>
    /// <param name="lst"></param>
    /// <param name="btn"></param>
    private void UseItem(List<IItem> lst,GameObject btn)
    {
        if(lst.Count > 0)
        {
            lst[0].UseItem();

            if (lst[0] is Weapon)
            {
                Debug.Log("arme équipée");
                stuffs[0].Add(lst[0]);
                GameObject btnWeapon = _invetoryCaseLeft[0];
                GameObject imgBtn = btnWeapon.transform.Find("Image").gameObject;
                GameObject text = btnWeapon.transform.Find("CounterItem").gameObject;
                imgBtn.GetComponent<Image>().sprite = lst[0].Sprite;
                MakeShowCaseInvetory(imgBtn.GetComponent<Image>(), text.GetComponent<Text>());
            }

            else if (lst[0] is Armor)
            {
                Debug.Log("armure équipée");
                stuffs[1].Add(lst[0]);
                GameObject btnArmor = _invetoryCaseLeft[1];
                GameObject imgBtn = btnArmor.transform.Find("Image").gameObject;
                GameObject text = btnArmor.transform.Find("CounterItem").gameObject;
                imgBtn.GetComponent<Image>().sprite = lst[0].Sprite;
                MakeShowCaseInvetory(imgBtn.GetComponent<Image>(), text.GetComponent<Text>());
            }

            else if (lst[0] is Head)
            {
                Debug.Log("casque équipée");
                stuffs[2].Add(lst[0]);
                GameObject btnHead = _invetoryCaseLeft[2];
                GameObject imgBtn = btnHead.transform.Find("Image").gameObject;
                GameObject text = btnHead.transform.Find("CounterItem").gameObject;
                imgBtn.GetComponent<Image>().sprite = lst[0].Sprite;
                MakeShowCaseInvetory(imgBtn.GetComponent<Image>(), text.GetComponent<Text>());
            }

            lst.RemoveAt(lst.Count - 1); // on supprime l'item a l'index max de la list
            if(lst.Count == 0)
            {
                GameObject imgBtn = btn.transform.Find("Image").gameObject;
                GameObject txtBtn = btn.transform.Find("CounterItem").gameObject;
                MakeHideCaseInvetory(imgBtn.GetComponent<Image>(), txtBtn.GetComponent<Text>());
            }
            else
            {
                // si la list n'est pas vide alor on change juste l'indicateur
                GameObject txtBtn = btn.transform.Find("CounterItem").gameObject;
                txtBtn.GetComponent<Text>().text = $"x{lst.Count}";
            }
        }
    }

    /// <summary>
    /// Update ui inventory case to hide case inventory
    /// </summary>
    public void HideCaseInventory(GameObject btn)
    {
        GameObject imgBtn = btn.transform.Find("Image").gameObject;
        GameObject txtBtn = btn.transform.Find("CounterItem").gameObject;
        MakeHideCaseInvetory(imgBtn.GetComponent<Image>(), txtBtn.GetComponent<Text>());
    }

    public void UpdateCaseInventory(GameObject btn, int itemCount)
    {
        // Get text button to update ui
        var txtBtn = btn.transform.Find("CounterItem").gameObject;
        // Update ui interface to clear inventory case
        txtBtn.GetComponent<Text>().text = $"x{itemCount}";
    }
}
