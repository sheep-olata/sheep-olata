﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

 [CreateAssetMenu(fileName = "EnemyData", menuName = "My Game/Enemy Data")]
public class EnemyData : ScriptableObject
{
    public string enemyName;
    public string description;
    public Sprite enemyModel;
    public int health = 100;
    public float speed = 1.5f;
    public int damage = 1;
}
