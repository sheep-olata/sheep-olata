﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script to grab and trow object in a map 
/// </summary>
public class GrabController : MonoBehaviour
{
    public bool grabbed;
    RaycastHit2D hit;
    public float distance = 2f;
    public Transform holdpoint;
    public float throwForce;
    public LayerMask notGrabbed;

    void Update()
    {
        // if player press "G"
        if (Input.GetKeyDown(KeyCode.G))
        {
            if (!grabbed)
            {
                Physics2D.queriesStartInColliders = false;
                hit = Physics2D.Raycast(transform.position, Vector2.right * transform.localScale.x, distance);

                if (hit.collider != null && hit.collider.tag == "Grabbable")
                {
                    grabbed = true;
                    hit.collider.gameObject.GetComponent<BoxCollider2D>().isTrigger = true;
                    hit.collider.gameObject.layer = LayerMask.NameToLayer("Grabbed");
                }
            }
            else if(!Physics2D.OverlapPoint(holdpoint.position, notGrabbed))
            {
                grabbed = false;
                hit.collider.gameObject.GetComponent<BoxCollider2D>().isTrigger = false;
                hit.collider.gameObject.layer = LayerMask.NameToLayer("Default");

                if (hit.collider.gameObject.GetComponent<Rigidbody2D>() != null)
                {
                    hit.collider.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(transform.localScale.x, 1);
                }
            }
        }
        if (grabbed)
        {
            hit.collider.gameObject.transform.position = holdpoint.position;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;

        Gizmos.DrawLine(transform.position, transform.position + Vector3.right * transform.localScale.x * distance);
    }
}
