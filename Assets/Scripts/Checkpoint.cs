﻿using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    private Transform PlayerSpawn;

    private void Awake()
    {
        PlayerSpawn = GameObject.FindGameObjectWithTag("PlayerSpawn").transform;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            PlayerSpawn.position = transform.position;
            Destroy(gameObject);
        }
    }    
}
