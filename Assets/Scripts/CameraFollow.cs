﻿using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public GameObject player; // Refers to the character
    public float timeOffset; // It is the time lag
    public Vector3 posOffset;

    private Vector3 velocity; // Necessary to use certain method of unity, we have to refer to this variable
    private void Start()
    {
        transform.position =  player.transform.position + posOffset;
    }
    void Update()
    {
        transform.position = Vector3.SmoothDamp(transform.position, player.transform.position + posOffset, ref velocity, timeOffset); // Permet de déplacer la caméra d'un endroit à un autre
    }
}
