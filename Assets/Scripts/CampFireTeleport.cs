﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CampFireTeleport : MonoBehaviour
{
    public GameObject _player; // le joueur
    public GameObject _panel; // le panel qui sera l'élément parent du bouton
    public GameObject[] _campFires; // les différents feux de camps de la scène configuer depuis Untity
    public GameObject m_GroupePage;
    public GameObject m_btnCampfirePrefab;
    public GameObject m_PageCampfirePrefab;
    public GameObject m_btnQuit;
    private GameObject m_currentPageTeleport;
    public List<GameObject> m_pages;
    private GameObject m_lastCampfire;
    /// <summary>
    /// On force le panneau de téléportation a la fermeture et on ajoute la fonction de le fermer au bouton "BtnQuit"
    /// Initialize the panel to set the buttons'parameters
    /// </summary>
    /// 
    private void Awake()
    {
        _panel.SetActive(false);
        GeneratePagesCampfire();
        GenerateBtn();
        m_btnQuit.GetComponent<Button>().onClick.AddListener(() => ClosePanel());
    }

    public bool IsCampFireActivate()
    {
        for (int i = 0; i < _campFires.Length; i++)
        {
            GameObject campFire = _campFires[i];

            if (campFire.GetComponent<CampFire>()._isActivate) // if the camfire is activate
            {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Methode allow to teleport the player to the chosen campfire
    /// </summary>
    /// <param name="tCampFire"></param>

    public void Teleport(Transform tCampFire)
    {
        _player.transform.position = tCampFire.position;
        Player.instance.setInvincibility();
        Player.instance.FullHeal();
        _panel.SetActive(false);
        StartCoroutine(CampFireTeleport.IsTeleportingCoroutine()); // Starts the coroutine created
        Time.timeScale = 1; // Restart game
    }

    /// <summary>
    /// Coroutine to change the player status to know when he is teleporting
    /// </summary>
    public static IEnumerator IsTeleportingCoroutine()
    {
        Player.instance.isTeleporting = true;

        // Waits 1 second before continuing the code
        yield return new WaitForSeconds(1);

        Player.instance.isTeleporting = false;
    }

    /// <summary>
    /// Methode can close the panel
    /// </summary>

    private void ClosePanel()
    {
        _panel.SetActive(false);
    }

    private void GeneratePagesCampfire()
    {
        //if(m_GroupePage.transform.childCount == 0)
        //{
        //    GameObject page = Instantiate(m_PageCampfirePrefab, m_GroupePage.transform);
        //    page.name = "page_1";
        //}
        for (int i = 0; i < _campFires.Length; i++)
        {
            if( i%8 == 0)
            {
                GameObject page = Instantiate(m_PageCampfirePrefab, m_GroupePage.transform);
                page.name = $"page_{m_GroupePage.transform.childCount}";
                page.SetActive(false);
            }
        }


        for (int i = 0; i < m_GroupePage.transform.childCount; i++)
        {
            m_pages.Add(m_GroupePage.transform.GetChild(i).gameObject);
        }
        m_pages[0].SetActive(true);
    }
    private void GenerateBtn()
    {
        int indexPage = 0;
        for (int i = 0; i < _campFires.Length; i++)
        {
            GameObject page = m_pages[indexPage];
            GameObject btn = null;
            CampFire campfire = _campFires[i].GetComponent<CampFire>();
            if (page.transform.GetChild(0).childCount < 4)//upside
            {
                btn = Instantiate(m_btnCampfirePrefab, page.transform.GetChild(0));
                btn.name = campfire._campfireName;
                btn.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = campfire._campfireName;
            }
            else if(page.transform.GetChild(0).childCount >= 4 && page.transform.GetChild(1).childCount < 4) //downside
            {
                btn = Instantiate(m_btnCampfirePrefab, page.transform.GetChild(1));
                btn.name = campfire._campfireName;
                btn.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = campfire._campfireName;
                if (page.transform.GetChild(1).childCount == 4)
                {
                    indexPage++;
                }
            }
            SetButton(btn, _campFires[i]);
        }
    }

    /// <summary>
    /// Method to set button with action on click and change text on button
    /// </summary>
    /// <param name="button"></param>
    /// <param name="campfire"></param>

    public void SetButton(GameObject button,GameObject campfire)
    {
        //Debug.Log($"{campfire.name}: {campfire.GetComponent<CampFire>()._isActivate}");
        if (campfire.GetComponent<CampFire>()._isActivate) // if the camfire is activate
        {
            Debug.Log($"{button.name}");
            button.transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>().text = campfire.GetComponent<CampFire>()._campfireName;
            button.GetComponent<Button>().onClick.AddListener(() => Teleport(campfire.transform));
            button.GetComponent<Button>().interactable = true; // we can interactable the button
            button.GetComponent<Image>().color = new Color(255f, 255f, 255f); // add white color on the button
        }
        else
        {
            button.transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>().text = campfire.GetComponent<CampFire>()._campfireName;
            button.GetComponent<Button>().onClick.AddListener(() => Teleport(campfire.transform));
            button.GetComponent<Button>().interactable = false; // we can't interactable the button
            button.GetComponent<Image>().color = new Color(127f, 127f, 127f); // add grey color on the button
        }
    }

    /// <summary>
    /// Method to update menu teleport
    /// </summary>
    public void UpdateTeleport()
    {
        int indexPage = 0;
        for (int i = 0; i < _campFires.Length; i++)
        {
            GameObject page = m_pages[indexPage];
            GameObject btn = null;
            GameObject campfire = _campFires[i];
            GameObject upSide = page.transform.GetChild(0).gameObject;
            GameObject downSide = page.transform.GetChild(1).gameObject;
            if (upSide.transform.childCount < 4)//upside
            {
                for (int j = 0; j < upSide.transform.childCount; j++)
                {
                    btn = upSide.transform.GetChild(j).gameObject;
                    if (btn.name.Equals(campfire.GetComponent<CampFire>()._campfireName))
                    {
                        SetButton(btn, campfire);
                    }
                }
            }
            else if (upSide.transform.childCount >= 4 && downSide.transform.childCount < 4) //downside
            {
                for (int j = 0; j < upSide.transform.childCount; j++)
                {
                    btn = upSide.transform.GetChild(j).gameObject;
                    SetButton(btn, campfire);
                }
                if (downSide.transform.childCount == 4)
                {
                    indexPage++;
                }
            }
        }
    }


    public void SetLastCampfire(GameObject newCampfire)
    {
        m_lastCampfire = newCampfire;
    }

    public GameObject GetLastCampFire()
    {
        return m_lastCampfire;
    }

}
