﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformAttack : MonoBehaviour
{
    public GameObject player;
    private void Start()
    {
        player = GameObject.FindWithTag("Player");
    }
    // on récupère le joueur
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject == player)
        {
           //si le joueur entre dans la zone d'ancrage
           // alors on récupère le transform parent du joueur
           // Cela permet transforme le transform du joeur en enfant du transfom "anchor"
            player.transform.parent = transform;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject == player)
        {
            // si le joueur quitte la zone d'ancrage alors
            // on récupère le transform parent du joueur et on lui retire l'affectation de l'ancrage 
            player.transform.parent = null;
        }
    }

}
