﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;


class Bat : Enemie
{
    public override string Name { get; protected set; }

    public override int HealtPoint { get; protected set; }
    public override int ShieldPoint { get; protected set; }
    public override int Level { get; protected set; }
    public override int ExpGain { get; protected set; }

    public override bool Speak { get; protected set; }
    public override bool Invincible { get; protected set; }

    public override float MoveSpeed { get; set; }
    public override float JumpForce { get; protected set; }
    public override int CurrentHealthPoint { get; protected set; }
    public override int CurrentShieldPoint { get; protected set; }
    public override int DamagePoint { get; protected set; }

    public DetectPlayer _detect;
    public AttackSpot _attackSpot;
    public MovementEntity movementEntity = new MovementEntity();
    public GameObject _parent, _healthBar,_position;
    public EnemyHealthBarMove enemyHealthBarMove;
    public List<RateItemDrop> m_poolItems;

    //private MobSpawner spawner;


    private void Start()
    {
        Initiate();
    }

    private void Update()
    {
        movementEntity.Move();
        enemyHealthBarMove.Move(_healthBar);
        if(Time.time > nextStrikeTime && _detect._isRange)
        {
            Attack();
            ActivateCooldown();
        }
    }

    public override void Die()
    {
        if (!IsLife())
        {
            StartCoroutine(DeadEnemie());
        }
        else
        {
            Debug.Log("L'enemie est vivant");
        }
    }
    public override void Speaking()
    {
        if (Speak)
        {

        }
    }
    public override void Attack()
    {
        if (_attackSpot._isRange)
        {
            try
            {
                _attackSpot._player.GetComponent<Player>().TakeDamage(DamagePoint);
            }
            catch
            {
                print("Je n'attaque pas cette cible");
            }
        }
    }
    public override void TakeDamage(int damage)
    {
        CurrentHealthPoint -= damage;
        if (CurrentHealthPoint > 0)
        {
            enemyHealthBarMove.healthBar.UdateHealthBar(CurrentHealthPoint);
            Debug.Log($"L'enemie prend {damage} il lui reste {CurrentHealthPoint}");
        }
        else if (CurrentHealthPoint <= 0)
        {
            CurrentHealthPoint = 0;
        }
        Die();
    }
    public override void Initiate()
    {
        Name = "Bat";
        HealtPoint = 100;
        CurrentHealthPoint = HealtPoint;
        ShieldPoint = 0;
        CurrentShieldPoint = ShieldPoint;
        Level = SetLevel();
        ExpGain = 15;
        DamagePoint = 5;
        MoveSpeed = 1.5f;
        JumpForce = 0;
        Speak = false;
        Invincible = false;
        _detect._isRange = false;
        _position = transform.gameObject;
        _parent = this.gameObject.transform.parent.gameObject;
        graphic = this.transform.gameObject.GetComponent<SpriteRenderer>();
        //spawner = this.gameObject.transform.parent.gameObject.transform.parent.gameObject.GetComponent<MobSpawner>();
        enemyHealthBarMove.healthBar.SetHeathBar(CurrentHealthPoint, HealtPoint);
        movementEntity.Initiate(_position, MoveSpeed, _detect, gameObject.GetComponent<SpriteRenderer>());
    }
    public override bool IsLife()
    {
        if (CurrentHealthPoint <= 0) // il c'est égale ou inférieur à 0
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    protected override int SetLevel()
    {
        int lvl = new GenerateNumber(levelMin,levelMax).GetNumber(); 
        Level = lvl;
        return Level;
    }


    protected override void PoolItems()
    {
        for (int i = 0; i < m_itemsDrop.Count; i++)
        {
            int pool = new GenerateNumber(0, m_itemsDrop.Count).GetNumber();
            m_poolItems.Add(m_itemsDrop[pool]);
        }
        DropItems();
    }

    public void DropItems()
    {
        for (int i = 0; i < m_poolItems.Count; i++)
        {
            int pool = new GenerateNumber(0, 100).GetNumber() ;
            if (pool <= m_poolItems[i].itemRate)
            {
                Debug.Log($"enemi drop an item {m_poolItems[i].item.name}");
                Instantiate(m_poolItems[i].item);
            }
        }
    }

    protected override void ActivateCooldown()
    {
        nextStrikeTime = Time.time + cooldownTime;
    }
    IEnumerator DeadEnemie()
    {
        int i = 0;
        gameObject.GetComponent<Animator>().enabled = false;
        movementEntity._moveSpeed = 0f;
        yield return new WaitForSeconds(0.1f);
        do
        {
            graphic.color = new Color(1f, 1f, 1f, 0f);
            yield return new WaitForSeconds(invincibilityFlashDelay);
            graphic.color = new Color(1f, 1f, 1f, 1f);
            yield return new WaitForSeconds(invincibilityFlashDelay);
            i++;
            yield return new WaitForSeconds(0.1f);

        } while (i < 5);
        print("l'enemie est mort");
        Player.instance.AddExpGain(this.ExpGain, this.Level);
        //spawner.ActivateCooldown();
        //spawner._mobIsAlive = false;
        PoolItems();
        Destroy(this.gameObject);
        yield return new WaitForSeconds(0.1f);
    }
}

