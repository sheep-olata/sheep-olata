﻿using System;
using UnityEngine;


public abstract class Boss : MonoBehaviour, ICharacters
{
    public abstract string Name { get; protected set; }
    public abstract int CurrentHealthPoint { get; protected set; }
    public abstract int HealtPoint { get; protected set; }
    public abstract int CurrentShieldPoint { get; protected set; }
    public abstract int ShieldPoint { get; protected set; }
    public abstract int Level { get; protected set; }
    public abstract int DamagePoint { get; protected set; }
    public abstract float MoveSpeed { get; protected set; }
    public abstract float JumpForce { get; protected set; }
    public abstract bool Speak { get; protected set; }
    public abstract bool Invincible { get; protected set; }
    public abstract float CoefResistance { get; protected set; }

    public abstract void Die();
    public abstract void Initiate();
    public abstract bool IsLife();
    public abstract void Speaking();
    public abstract void TakeDamage(int damage);
}