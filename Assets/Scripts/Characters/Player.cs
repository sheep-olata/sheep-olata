﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public struct PlayerData//serializable struct containing useful data from Player object for save/load
{

    public string Name;

    public int HealtPoint;
    public int ShieldPoint;
    public int Level;
    public int ExpGain;
    public int CurrentHealthPoint;
    public int CurrentShieldPoint;
    public int DamagePoint;

    public bool Speak;
    public bool Invincible;

    public float MoveSpeed;
    public float JumpForce;

    public int _expMax;
    public int _currentExp;

    public Vector3 position;


}
public class Player : MonoBehaviour,ICharacters
{

    public static Player instance;

    public string Name { get; private set; }

    public int HealtPoint { get; set; }
    public int ShieldPoint { get; set; }
    public int Level { get; private set; }
    public int ExpGain { get; private set; }
    public int CurrentHealthPoint { get; set; }
    public int CurrentShieldPoint { get; set; }
    public int DamagePoint { get; set; }

    public bool Speak { get; private set; }
    public bool Invincible { get; private set; }

    public float MoveSpeed { get; set; }
    public float JumpForce { get; set; }

    public int _expMax { get; private set; }
    public int _currentExp { get; set; }

    public HealthBar healthBar = new HealthBar();
    public ShieldBar shieldBar = new ShieldBar();
    public ExpBar expBar = new ExpBar();
    public SpriteRenderer graphic;
    public DetectEnemie detectEnemie;
    public DetectEnvironment detectEnvironment;

    public Vector3 m_initiateSpawn;
    public float invincibilityTimeAfterHit = 0.5f;
    public float invincibilityFlashDelay = 0.15f;

    public float respawnInvincibilityDelay = 3.0f; // Invincibility duration for respawn / teleportation. Set at 3.0f by default
    public bool isInvincible = false;
    public bool isTeleporting = false;
    public bool m_hasConsumeGrass = false;

    public GameObject freshBreath;
    public GameObject poisonusGas;

    [SerializeField] private Enemie m_target;
    private string m_sideDirection = "Right";

    Renderer rend;
    Color c;

    //public Text levelIndicator;
    public void Awake()
    {
        if (instance != null)
        {
            Debug.Log("Il y'a plus d'une instance de Player dans la scene");
            return;
        }
        instance = this;
        Initiate();
    }

    private void Update()
    {
        SetSide();
        DetectEnemie();

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            StartCoroutine(AttackBase());

        }
        else if (Input.GetKeyDown(KeyCode.G) && m_hasConsumeGrass)
        {
            Debug.Log("ici");
            if(m_target != null)
            {
                GameObject _freshBreath = Instantiate(freshBreath,null);
                _freshBreath.GetComponent<FreshBreath>().InitAttack(this);
                _freshBreath.transform.position = m_target.transform.position;
            }
        }
        else if (Input.GetKeyDown(KeyCode.A) && m_target != null)
        {
            Debug.Log("ici");
            GameObject _poisonusGas = Instantiate(poisonusGas, null);
            _poisonusGas.GetComponent<PoisonousGas>().InitAttack(this);
            _poisonusGas.transform.position = m_target.transform.position;
        }
        else if (Input.GetKeyDown(KeyCode.T) && detectEnvironment._isRangeObject)
        {
            InteractObject();
        }
        /*
        if(Input.GetKeyDown(KeyCode.R))
        {
            Debug.Log("Damage Points : " + DamagePoint);
            Debug.Log("Shield Points : " + ShieldPoint);
            Debug.Log("Shield Points2 : " + CurrentShieldPoint);
        } */
    }
    #region save/load
    public void LoadFromPlayerData(PlayerData data)//Load variables from a serializable struct into the object
    {
        HealtPoint = data.HealtPoint;
        ShieldPoint = data.ShieldPoint;
        Level = data.Level;
        ExpGain = data.ExpGain;
        CurrentHealthPoint = data.CurrentHealthPoint;
        CurrentShieldPoint = data.CurrentShieldPoint;
        DamagePoint = data.DamagePoint;
        Speak = data.Speak;
        Invincible = data.Invincible;
        MoveSpeed = data.MoveSpeed;
        JumpForce = data.JumpForce;
        _expMax = data._expMax;
        _currentExp = data._currentExp;
        transform.position = data.position;

        //update health UI
        shieldBar.UpdateShield(CurrentShieldPoint);
        healthBar.UdateHealthBar(CurrentHealthPoint);
    }
    public PlayerData GetPlayerDataFromPlayer()//get all variables from the object and put them in a serializable struct
    {
        PlayerData data = new PlayerData();

        data.HealtPoint = HealtPoint;
        data.ShieldPoint = ShieldPoint;
        data.Level = Level;
        data.ExpGain = ExpGain;
        data.CurrentHealthPoint = CurrentHealthPoint;
        data.CurrentShieldPoint = CurrentShieldPoint;
        data.DamagePoint = DamagePoint;
        data.Speak = Speak;
        data.Invincible = Invincible;
        data.MoveSpeed = MoveSpeed;
        data.JumpForce = JumpForce;
        data._expMax = _expMax;
        data._currentExp = _currentExp;
        data.position = transform.position;

        return data;
    }
    #endregion
    public void Die()
    {
        if (!IsLife())
        {
            Debug.Log("Le personnage est mort");
            //DeathMenu.instance.ShowMenu();
            GameManager.instance.GetMenuManager().OpenMenuByName("Death");
            CurrentHealthPoint = 0;
            healthBar.UdateHealthBar(CurrentHealthPoint);
        }
        else
        {
            Debug.Log("Le personnage est vivant");
        }
    }


    public void Speaking()
    {
        if (Speak)
        {

        }
    }

    public void Initiate()
    {
        Name = "Olata";
        HealtPoint = 100;
        CurrentHealthPoint = HealtPoint;
        ShieldPoint = 20;
        CurrentShieldPoint = ShieldPoint;
        Level = 1;
        MoveSpeed = 150;
        JumpForce = 250;
        DamagePoint = 2;
        ExpGain = 0;
        _expMax = 100;
        healthBar.SetHeathBar(this.HealtPoint, this.CurrentHealthPoint);
        shieldBar.SetShieldBar(this.CurrentShieldPoint, this.ShieldPoint);
        expBar.SetExpBar(_currentExp, _expMax);
        graphic = this.transform.gameObject.GetComponent<SpriteRenderer>();
        m_initiateSpawn = gameObject.transform.position;
        StartCoroutine(CampFireTeleport.IsTeleportingCoroutine()); // Calls the coroutine for the spawn

        rend = GetComponent<Renderer>();
        c = rend.material.color;

        //levelIndicator.text = Level.ToString();
    }

    public bool IsLife()
    {
        if (CurrentHealthPoint <= 0) // il c'est égale ou inférieur à 0
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    /// <summary>
    /// Méthode qui permet de faire monter d'un niveau le joueur
    /// </summary>
    public void LevelUp()
    {
        Debug.Log($"{this.Name} a gagner un niveau !");
        Level += 1;
        HealtPoint += 10;
        healthBar.SetHeathBar(CurrentHealthPoint, HealtPoint);
        _expMax += (_expMax + 100);
        expBar.SetExpBar(_currentExp, _expMax);
        _currentExp = 0;
        //levelIndicator.text = Level.ToString();
    }

    /// <summary>
    /// Méthode qui donne de l'exp au joueur
    /// </summary>
    /// <param name="expGain"></param>
    public void AddExpGain(int expGain,int enemieLvl)
    {
        int exp = AdjustExpGain(expGain, enemieLvl);
        _currentExp += exp;
        if (_currentExp > _expMax)
        {
            int rest = _currentExp - _expMax;
            LevelUp();
            _currentExp += rest;
            expBar.UpDateEXPBar(_currentExp);
        }
        else if (_currentExp == _expMax)
        {
            LevelUp();
            expBar.UpDateEXPBar(_currentExp);
        }
        else
        {
            expBar.UpDateEXPBar(_currentExp);
        }
        print($"le joueur a reçut {exp} point d'exeprience");
    }

    // Function to call the Coroutine to activate the invincibility 
    public void setInvincibility()
    {
        StartCoroutine("GetInvulnerable");
    }

    // Enumerator for the Coroutine
    IEnumerator GetInvulnerable()
    {
        isInvincible = true; // Sets the invincibility
        c.a = 0.5f; // Makes the player render semi transparent
        rend.material.color = c;

        yield return new WaitForSeconds(respawnInvincibilityDelay); // Waits 3 seconds (delay)
        isInvincible = false; // Turns off the invincibility
        c.a = 1.0f; // Restore player render to normal
        rend.material.color = c;
    }

    /// <summary>
    /// Méthode qui permet au joueur de reçevoir des dégats
    /// </summary>
    /// <param name="damage"></param>
    public void TakeDamage(int damage)
    {
        if(!isInvincible) 
        {
            if (CurrentShieldPoint > 0) // TODO retenu dégât shield si (shield - dégât) < 0 et l'appliquer sur la vie
            {
                CurrentShieldPoint -= damage;
                shieldBar.UpdateShield(CurrentShieldPoint);

            }
            else
            {
                CurrentHealthPoint -= damage;
                if (CurrentHealthPoint > 0)
                {
                    healthBar.UdateHealthBar(CurrentHealthPoint);
                    Debug.Log($"Le joueur prend {damage} il lui reste {CurrentHealthPoint}");
                }
                else if (CurrentHealthPoint <= 0)
                {
                    CurrentHealthPoint = 0;
                }
            }
            Die();
        }
        
    }

    /// <summary>
    /// Function to take fall damage (player)
    /// </summary>
    /// <param name="damage"></param>
    public void TakeFallingDamage(int damage)
    {   
        decimal damagePercentage = ((decimal)damage / 100); // Get the decimal percentage of the damage (for example 20% would be 0.2, 2% would be 0.02)
        decimal remainingHealthPercentage = (1 - damagePercentage); // Get the decimal percentage of the hp (for example 98% of hp would be 0.98)
        decimal convertedHealthPoint = (decimal)CurrentHealthPoint; // Converts the CurrentHealthPoint from int to dec
        decimal remainingHealthPoint = convertedHealthPoint * remainingHealthPercentage; // Calculates remaining health point dec of current health * percentage)
        int valueDmg = CurrentHealthPoint - decimal.ToInt32(remainingHealthPoint); // Checks how much numerical (int) damage the player took for printing purpose

        CurrentHealthPoint = decimal.ToInt32(remainingHealthPoint); // Converts the remaining health back to int and updates CurrentHealthPoint

        if (!isInvincible)
        {
            if (CurrentHealthPoint > 0)
            {
                healthBar.UdateHealthBar(CurrentHealthPoint);
                Debug.Log($"Le joueur prend {valueDmg} il lui reste {CurrentHealthPoint}");
            }

            else if (CurrentHealthPoint <= 0)
            {
                CurrentHealthPoint = 0;
            }

            Die();
        }

    }

    public void Attack(int damage,GameObject target)
    {
        try
        {
            Enemie enemie = target.GetComponent<Enemie>();
            enemie.TakeDamage(damage);
        }
        catch
        {
            print("il y'a une erreur de récupération de script");
        }
    }
    /// <summary>
    /// Adjust EXP obtain related Olata's level
    /// </summary>
    /// <param name="expGain"></param>
    /// <param name="enemieLvl"></param>
    /// <returns></returns>
    private int AdjustExpGain(int expGain,int enemieLvl)
    {
        if(enemieLvl != 0)
        {
            double difference = this.Level - enemieLvl;
            double coef = ((difference/10) * (-1)) + 1;
            double newExpGain = expGain * coef;
            return (int)newExpGain;
        }
        else
        {
            return expGain;
        }

    }
    public void FullHeal()
    {
        CurrentHealthPoint = HealtPoint;
        CurrentShieldPoint = ShieldPoint;
        this.healthBar.UdateHealthBar(CurrentHealthPoint);
        this.shieldBar.UpdateShield(CurrentShieldPoint);
    }

    public void DetectEnemie()
    {
        int layerPlayer = 8;
        int layerMask = ~(1 << layerPlayer);
        if (m_sideDirection == "Right")
        {
            RaycastHit2D hit = Physics2D.Raycast(gameObject.transform.position, Vector3.right,1f, layerMask);
            Debug.DrawRay(gameObject.transform.position, gameObject.transform.TransformDirection(Vector3.right));
            //Debug.DrawRay(gameObject.transform.position, gameObject.transform.TransformDirection(Vector3.right));
            if (hit.collider != null && !hit.collider.CompareTag("Enemie") && !hit.collider.CompareTag("EffectAttack"))
            {
                m_target = hit.collider.gameObject.transform.parent.transform.gameObject.GetComponent<Enemie>();
            }
            else
            {
                m_target = null;
            }
        }
        else if (m_sideDirection == "Left")
        {
            RaycastHit2D hit = Physics2D.Raycast(gameObject.transform.position, gameObject.transform.TransformDirection(Vector3.left),1f, layerMask);
            Debug.DrawRay(gameObject.transform.position, gameObject.transform.TransformDirection(Vector3.left));
            if (hit.collider!=null && hit.collider != null && !hit.collider.CompareTag("Enemie") && !hit.collider.CompareTag("EffectAttack"))
            {
                 hit.collider.gameObject.transform.parent.transform.gameObject.TryGetComponent<Enemie>(out m_target);
            }
            else
            {
                m_target = null;
            }
        }
    }

    IEnumerator AttackBase()
    {
        gameObject.GetComponent<Animator>().SetTrigger("Attack");
        yield return new WaitForSeconds(0.5f);
        Attack(DamagePoint, detectEnemie._enemie);
        yield return new WaitForSeconds(0.1f);
    }


    public void InteractObject()
    {
        DestroyObject destroyObject = detectEnvironment._crate.GetComponent<DestroyObject>();
        destroyObject.TakeHit();
    }

    private void SetSide()
    {
        if(GameManager.instance.GetControlManager().GetInputDirection().x == -1)
        {
            m_sideDirection = "Left";
        }else if(GameManager.instance.GetControlManager().GetInputDirection().x == 1)
        {
            m_sideDirection = "Right";
        }
    }

    public void SetHasEatGrass(bool newBool)
    {
        m_hasConsumeGrass = newBool;
    }

}
