﻿public interface ICharacters
{
    string Name { get; }

    int CurrentHealthPoint { get; }
    int HealtPoint { get; }
    int CurrentShieldPoint { get; }
    int ShieldPoint { get; }
    int Level { get; }
    int DamagePoint { get; }


    float MoveSpeed { get; }
    float JumpForce { get; }

    bool Speak { get; }
    bool Invincible { get; }

    void Die();
    bool IsLife();
    void Speaking();
    void Initiate();
    void TakeDamage(int damage);
}
