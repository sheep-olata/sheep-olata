﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public abstract class Enemie : MonoBehaviour, ICharacters
{
    public abstract int HealtPoint { get; protected set; }
    public abstract string Name { get; protected set; }
    public abstract int ShieldPoint { get; protected set; }
    public abstract int Level { get; protected set; }
    public abstract int ExpGain { get; protected set; }

    public abstract bool Speak { get; protected set; }
    public abstract bool Invincible { get; protected set; }

    public abstract float MoveSpeed { get; set; }
    public abstract float JumpForce { get; protected set; }
    public abstract int CurrentHealthPoint { get; protected set; }
    public abstract int CurrentShieldPoint { get; protected set; }
    public abstract int DamagePoint { get; protected set; }

    //public HealthBar healtBar;
    //public ShieldBar shieldBar;

    public List<RateItemDrop> m_itemsDrop = new List<RateItemDrop>();



    public float cooldownTime;
    public float nextStrikeTime;

    public int levelMin;
    public int levelMax;
    public float invincibilityFlashDelay;
    public SpriteRenderer graphic;
    

    public abstract void Speaking();
    public abstract void Attack();
    public abstract void TakeDamage(int damage);
    public abstract void Initiate();
    public abstract bool IsLife();
    public abstract void Die();
    protected abstract int SetLevel();
    protected abstract void PoolItems();
    protected abstract void ActivateCooldown();

    

}
