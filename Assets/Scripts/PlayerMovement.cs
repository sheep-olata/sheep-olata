﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Yarn.Unity;
using Yarn.Unity.Sheepolata;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Animator))]
public class PlayerMovement: MonoBehaviour
{
    #region condition Platform
    public float _coefSlidely;
    private float _currentHorizontalMovement;
    private bool isStiky = false;
    private bool isSlide = false;
    #endregion

    public bool m_isGrounded; // indicate if player is on ground
    private bool m_isJumping; // indicate if player is jumping
    
    [Header("jump condition")]
    [SerializeField]
    private bool m_canJump = false;  // allow to player able jump
    [SerializeField]
    private bool m_doubleJumpAllow = false;  // allow to player a double jump

    private bool m_checkRightGround = false, m_checkLeftGround = false;  // detect 2 sides of player
    private float m_inputMovement   = 0.0f;                              // input player -1 left direction and +1 right direction
    [Header("Movement")]
    [SerializeField]
    private float m_moveSpeed = 150.0f,m_jumpForce = 5.0f;
    [Header("Materials 2D")]
    [SerializeField]
    private PhysicsMaterial2D fullFriction, noneFriction; // 2 physics materials applied when player is slope or not

    private float stepTimer;

    #region Components
    // all component is instanciate in Start function
    private Rigidbody2D rb; // Refers to the character's rigidbody for movement
    private Animator animator; // Refers to the animation created in Unity
    private SpriteRenderer spriteRenderer; // Component that corresponds to the visual of the character    
    #endregion

    private Vector3 velocity = Vector3.zero;  // Neccesaire pour utiliser certaine méthode de unity, on doit faire référence à cette variable
    public LayerMask collisionLayer;

    // Fall damage amount
    private float m_maxYVelo; // Maximum Y-axis to take damage
    bool jumpingFall; // Boolean to check if player is jumping for fall damage
    [Header("Fall Damage Velocity")]
    [SerializeField]
    private int velFallX = -7, velFallY = -9, velFallDie = -10;  // Fall velocity on Y-Axis for variable velFallX and velFallY || Fall velocity on Y-Axis to die
    [Header("Fall Damage Percentage")]
    [SerializeField]
    private int lowFallDamage = 5, highFallDamage = 100;  // The fall damage to take

    public float durationRunning = 5.0f;

    [Header("GameObject")]
    [SerializeField]
    List<GameObject> m_groundCheckSpots;
    public GameObject canvaRespawn;
    public GameObject panelRespawn;
    private Vector3 respawnPoint;
    private bool m_isOnSlope = false;
    // Get Audio files
    [Header("Audio")]
    public AudioSource audioSource;
    public List<AudioClip> jumpingSounds;
    public List<AudioClip> walkingSounds;
    public List<AudioClip> landingSounds;
    public FootstepSet DefaultFootstepSet;
    [SerializeField]
    private AudioClip doubleJumpSound;


    // Dialogue Yarn spinner 
    public float interactionRadius = 2.0f;

    private float cooldownRunning;
    private float cooldownResetJump;
    private float cooldownDodge;

    private float delayResetJump = 0.8f;
    private float delayDodge = 0.5f;
    private bool m_isRunning = false;

    public bool m_isSwimming = false;

    private void Start()
    {
        respawnPoint = transform.position;  // Set the first respawn point by the first position on the player
        if(TryGetComponent(out Rigidbody2D rBody)) { rb = rBody; }
        if(TryGetComponent(out Animator anim)) { animator = anim; }
        if(TryGetComponent(out SpriteRenderer sprt)) { spriteRenderer = sprt; }
        audioSource = GetComponent<AudioSource>();
        if(DefaultFootstepSet == null)
        {
            Debug.LogWarning("PlayerMovement requires a DefaultSoundSet to work properly");
        }
        ChangeFootstepSet(DefaultFootstepSet);//initialize the current footstepset to the default
    }

    /// <summary>
    /// Permet d'appeler qu'une seul fois le script dans tous le jeu etant donnée qu'il est seul "singletone"
    /// </summary>
    void Update()
    {
        // Remove all player control when we're in dialogue
        if (FindObjectOfType<DialogueRunner>().IsDialogueRunning == true)
        {
            return;
        }
        stepTimer -= Time.deltaTime;
        // Detect if we want to start a conversation
        if (Input.GetKeyDown(KeyCode.F))
        {
            CheckForNearbyNPC();
        }

        InputPlayer();
        CheckSlope();
        SetIsGrounded();
        ResetJump();
        ResetInvisibility();
        AnimationSwimming();
    }

    void FixedUpdate()
    {

        MovePlayer();
        //Jump();
        float charaterVelocity = Mathf.Abs(rb.velocity.x);  // Allows to always return a positive value, even if the character goes backwards

        animator.SetFloat("Speed", charaterVelocity);

        if (m_isOnSlope && m_inputMovement == 0)
        {
            rb.sharedMaterial = fullFriction;
        }
        else
        {
            rb.sharedMaterial = noneFriction;
        }

        TakeFallDamage();
    }

    private void LateUpdate()
    {
        Flip(rb.velocity.x); // On lui envoi la vitesse sur la x avec des valeur positive ou négative (postive, le personnage va vers l'avant, négative, il va vers la gauche)
    }

    /// <summary>
    /// Method to move the character
    /// </summary>
    void MovePlayer()
    {
        if (!m_isRunning) 
        {
            Vector3 targetVelocity = new Vector2(m_inputMovement * Time.fixedDeltaTime * m_moveSpeed, rb.velocity.y);  // The direction we are moving based on _horizontalMotion (on the x-axis), and on the default value of the rigidbody (on the y-axis)
                    rb.velocity    = Vector3.SmoothDamp(rb.velocity, targetVelocity, ref velocity, .05f);              // Then the movement is executed
        }
        else
        {
            Vector3 targetVelocity = new Vector2(m_inputMovement * Time.fixedDeltaTime * (m_moveSpeed * 1.5f), rb.velocity.y);  // The direction we are moving based on _horizontalMotion (on the x-axis), and on the default value of the rigidbody (on the y-axis)
            rb.velocity = Vector3.SmoothDamp(rb.velocity, targetVelocity, ref velocity, .05f);              // Then the movement is executed
        }

        if (isSlide) // if player is sliding on platform (like frozen platform)
        {
            float result = 0f;
            if (_currentHorizontalMovement < 0f)
            {
                result = _coefSlidely * -1;
            }
            if(_currentHorizontalMovement > 0f)
            {
                result = _coefSlidely * 1;
            }
            rb.AddForce(new Vector2(result, 0f));
        }
        if (rb.velocity.magnitude > 0.1 && stepTimer < 0 && m_isGrounded)
        {
            int randomSound = Random.Range(0, walkingSounds.Count - 1);
            stepTimer = 0.4f;
            audioSource.PlayOneShot(walkingSounds[randomSound]);
        }
    }
    /// <summary>
    /// Method to flip the character when he walks to the left
    /// </summary>
    void Flip(float _velocity)
    {
        if (_velocity > 0.1f) // The character goes forward
        {
            spriteRenderer.flipX = false;
        }
        else if (_velocity < -0.1f) // The character goes to the left
        {
            spriteRenderer.flipX = true;
        }
    }

    public void SetIsGrounded()
    {
        CheckLeftPartGrounded();
        CheckRightPartGrounded();
        if ((m_checkLeftGround || m_checkRightGround) && !m_isJumping)
        {
            m_isGrounded = true;
            m_isJumping = false;
            animator.SetBool("isJumping",false);
        }
        else
        {
            m_isGrounded = false;
        }
    }

    private void CheckLeftPartGrounded()
    {
        RaycastHit2D hitLeft = Physics2D.Raycast(m_groundCheckSpots[0].transform.position, Vector2.down, .10f, collisionLayer, 0, 0);
        if (hitLeft.collider != null)
        { 
            if (hitLeft.collider.tag.Equals("Ground"))
            {
                m_checkLeftGround = true;
            }
            else
            {
                m_checkLeftGround = false;
            }
        }
        else
        {
            m_checkLeftGround = false;
        }
    }

    private void CheckRightPartGrounded()
    {
        RaycastHit2D hitRight = Physics2D.Raycast(m_groundCheckSpots[1].transform.position, Vector2.down, .10f, collisionLayer, 0, 0);

        if (hitRight.collider != null)
        {
            if (hitRight.collider.tag.Equals("Ground"))
            {
                m_checkRightGround = true;
            }
            else
            {
                m_checkRightGround = false;
            }
        }
        else
        {
            m_checkRightGround = false;
        }
    }

    void InputPlayer()
    {
        m_inputMovement = GameManager.instance.m_controller.GetInputDirection().x;

        if (GameManager.instance.m_controller.JumpInputPressed() && m_canJump)
        {
            Jump();
        }
        if (Input.GetKey(KeyCode.LeftShift))
        {
            m_isRunning = true;
            SetCooldownRunning();
        }else if(cooldownRunning < Time.time || cooldownRunning == 0f)
        {
            m_isRunning = false;
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            animator.SetTrigger("IsDodge");
            Player.instance.isInvincible = true;
            SetCoodownInvicibilityDodge();
        }
    }

    public void Jump()
    {
        if (m_canJump && m_isGrounded == true)
        {
            rb.AddForce(Vector2.up * m_jumpForce, ForceMode2D.Impulse);
            SetCooldownResetJump();
            int randomSound = Random.Range(0, jumpingSounds.Count - 1);

            audioSource.PlayOneShot(jumpingSounds[randomSound]);
            m_isJumping = true;
            animator.SetBool("isJumping", true);
            //StartCoroutine(JumpAnim());
        }

        if (m_doubleJumpAllow && m_isGrounded == false && m_isJumping == true)
        {
            rb.velocity = new Vector2(0f, 0f);
            rb.AddForce(Vector2.up * m_jumpForce / 1.3f, ForceMode2D.Impulse);
            cooldownResetJump = 0.0f;
            animator.Play("Base Layer.PlayerJump",0);
            int randomSound = Random.Range(0, jumpingSounds.Count - 1);

            audioSource.PlayOneShot(doubleJumpSound);
            m_isJumping = false;
        }
    }

    public void Run()
    {
        Vector3 targetVelocity = new Vector2(m_inputMovement * Time.fixedDeltaTime * m_moveSpeed, rb.velocity.y);  // The direction we are moving based on _horizontalMotion (on the x-axis), and on the default value of the rigidbody (on the y-axis)
        rb.velocity = Vector3.SmoothDamp(rb.velocity, targetVelocity, ref velocity, .05f);
    }

    public void CheckSlope()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, 1f, collisionLayer);
        Debug.DrawRay(transform.position, Vector2.down,Color.blue);
        if (hit.collider != null && Mathf.Abs(hit.normal.x) > 0.1f)
        {
            float angle = Vector2.Angle(hit.normal, Vector2.up);
            if(hit.normal.x > 0 && m_isGrounded)
            {
                transform.rotation = Quaternion.Euler(0f, 0f, -angle);
            }
            else if(hit.normal.x < 0 && m_isGrounded)
            {
                transform.rotation = Quaternion.Euler(0f, 0f, angle);
            }
            m_isOnSlope = true;
        }
        else
        {
            m_isOnSlope = false;
            transform.rotation = Quaternion.Euler(0f, 0f, 0f);
        }
    }

    IEnumerator JumpAnim()
    {
        animator.SetBool("isJumping", true);
        yield return new WaitForSeconds(0.1f);
        
        // Play jumping sound
        int randomSound = Random.Range(0, jumpingSounds.Count - 1);
        
            audioSource.PlayOneShot(jumpingSounds[randomSound]);
        

        m_isJumping = true;
        yield return new WaitForSeconds(1f);
        animator.SetBool("isJumping", false);
        yield return new WaitForSeconds(0.5f);
    }

    /// <summary>
    /// Method to respawn the player when he fall
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "FallDetector")
        {
            Player.instance.CurrentHealthPoint = 0;
            Player.instance.Die();
        }
    }

    /// <summary>
    /// Function to calculate when to take fall damage
    /// </summary>
    public void TakeFallDamage()
    {
        if (jumpingFall && rb.velocity.y == 0)
        {
            jumpingFall = false;

            if (m_maxYVelo <= velFallX && m_maxYVelo >= velFallY) // Takes damage when between velFallX and velFallY velocity (recommended values of -6 and -9)
            {
                FallDamageAmount(lowFallDamage); // Percentage of the current health to take as damage
            }

            else if (m_maxYVelo <= velFallDie) // Dies if velocity is equal to or bigger than velFallDie (recommended value of -10)
            {
                FallDamageAmount(highFallDamage); // Percentage of the current health to take as damage
            }

        }

        else if (jumpingFall)
        {
            if (rb.velocity.y < m_maxYVelo)
            {
                m_maxYVelo = rb.velocity.y;
            }
        }

        if (!jumpingFall && m_isJumping)
        {
            jumpingFall = true;
            m_maxYVelo  = 0;     // Resets the max Y-axis velocity to avoid the take damage multiple times in flight
        }
    }

    /// <summary>
    /// Function to call the TakeFallingDamage function from the Player.cs script
    /// </summary>
    /// <param name="damage"></param>
    public void FallDamageAmount(int damage)
    {
            Player.instance.TakeFallingDamage(damage); 
        /// Draw the range at which we'll start talking to people.
        void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.blue;

            // Flatten the sphere into a disk, which looks nicer in 2D games
            Gizmos.matrix = Matrix4x4.TRS(transform.position, Quaternion.identity, new Vector3(1, 1, 0));

            // Need to draw at position zero because we set position in the line above
            Gizmos.DrawWireSphere(Vector3.zero, interactionRadius);
        }
    }

    /// Find all DialogueParticipants
    /** Filter them to those that have a Yarn start node and are in range; 
     * then start a conversation with the first one
     */
    public void CheckForNearbyNPC()
    {
        var allParticipants = new List<NPC>(FindObjectsOfType<NPC>());
        var target          = allParticipants.Find(delegate (NPC p) {
            return string.IsNullOrEmpty(p.talkToNode) == false && // has a conversation node?
            (p.transform.position - this.transform.position)// is in range?
            .magnitude <= interactionRadius;
        });
        if (target != null)
        {
            // Kick off the dialogue at this node.
            FindObjectOfType<DialogueRunner>().StartDialogue(target.talkToNode);
        }
    }

    public void ChangeFootstepSet(FootstepSet _set)//changes the currently loaded sound set, called by SoundZone.cs
    {
        jumpingSounds = _set.Jumps;
        walkingSounds = _set.Steps;
        landingSounds = _set.Lands;
    }

    private void SetCooldownRunning()
    {
        cooldownRunning = Time.time + durationRunning;
    }

    private void SetCooldownResetJump()
    {
        cooldownResetJump = Time.time + delayResetJump;
    }

    private void SetCoodownInvicibilityDodge()
    {
        cooldownDodge = delayDodge + Time.time;
    }

    private void ResetInvisibility()
    {
        if(Time.time > cooldownDodge)
        {
            Player.instance.isInvincible = false;
        }
    }

    private void ResetJump()
    {
        if(Time.time > cooldownResetJump && cooldownResetJump != 0.0f)
        {
            m_isJumping = false;
            cooldownResetJump = 0.0f;
        }
    }

    public void AnimationSwimming()
    {
        if (m_isSwimming)
        {
            animator.SetBool("IsSwimming", true);
        }
        else
        {
            animator.SetBool("IsSwimming", false);
        }
    }
}

