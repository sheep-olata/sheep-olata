﻿
using UnityEngine;

public class HealPowerUp : MonoBehaviour
{
    public int healthPoint;
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if(PlayerHealth.instance.currentHealth < PlayerHealth.instance.maxHealth)
            {
                //PlayerHealth.instance.HealPlayer(healthPoint);
                Destroy(gameObject);
            }
        }
    }
}
