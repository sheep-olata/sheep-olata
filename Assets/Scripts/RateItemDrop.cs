﻿using UnityEngine;

[System.Serializable]
public struct RateItemDrop 
{
    public GameObject item;
    public float itemRate;
}
