﻿using System;

class GenerateNumber
{
    private int _max;
    private int _min;
    private Random _rand;
    public GenerateNumber(int min,int max)
    {
        _max = max;
        _min = min;
        _rand = new Random();
    }

    public int GetNumber()
    {
        int result = _rand.Next(_min,_max);
        return result;
    }
}
