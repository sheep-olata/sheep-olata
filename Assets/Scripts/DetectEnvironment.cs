﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



[Serializable]
/// <summary>
/// This class can detect his environment's player
/// </summary>
public class DetectEnvironment : MonoBehaviour
{
    public GameObject _indicator;
    public GameObject _indicator2;
    public GameObject _tpMenu;
    public GameObject _canvas;
    public GameObject _indicatorSave;

    public GameObject _crate;


    public float _indicatorDelay;
    public float _indicatorDelaySave;
    public bool _isRange = false;
    public bool _isRangeObject = false;

    public Vector3 respawnPoint;
    public bool isInLava = false;
    public float lavaDelay = 3.0f;


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && _isRange && !_tpMenu.activeSelf)
        {
            _tpMenu.SetActive(true);
            Player.instance.FullHeal();
            StartCoroutine(CampFireTeleport.IsTeleportingCoroutine()); // Calls the coroutine for the spawn
        }
        else if (Input.GetKeyDown(KeyCode.E) && _isRange && _tpMenu.activeSelf)
        {
            _tpMenu.SetActive(false);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("CampFire")) // if oject has tag "CampFire"
        {
            if (!collision.GetComponent<CampFire>()._isActivate) // if campfire is not activate
            {
                _indicator2.GetComponent<Text>().text = "Feu de camp activé"; // change text indicator
                collision.GetComponent<CampFire>()._isActivate = true; // activate campfire
                StartCoroutine(AnimationIndicator()); // start animation
                _canvas.GetComponent<CampFireTeleport>().SetLastCampfire(collision.gameObject);
            }
            else
            {
                StartCoroutine(AnimationSave()); // start animation save text
                SaveManager.SaveGame(FindObjectOfType<Player>().GetPlayerDataFromPlayer(), FindObjectOfType<Inventaire>().GetInventaireDataFromInventaire(),SceneManager.GetActiveScene().buildIndex);//save function
                _canvas.GetComponent<CampFireTeleport>().SetLastCampfire(collision.gameObject);
            }
            _indicator.SetActive(true); // activate indicator
            _indicator.GetComponent<Text>().text = "Appuyer sur \"E\" pour vous téléporter"; // change text indicator
            _canvas.GetComponent<CampFireTeleport>().UpdateTeleport(); // update panel
            _isRange = true;
        }
        else if (collision.CompareTag("ObjectInteract"))
        {
            _isRangeObject = true;
            _crate = collision.gameObject;
        }

        else if (collision.tag == "Lavadamage")
        {
            isInLava = true;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Lavadamage")
        {
            lavaDelay -= 0.1f;

            if (isInLava)
            {
                if (lavaDelay < 0)
                {
                    Player.instance.TakeDamage(5);
                    lavaDelay = 2.0f;
                }
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("CampFire"))
        {
            _indicator.SetActive(false);
            StartCoroutine(CampFireTeleport.IsTeleportingCoroutine()); // Calls the coroutine for the spawn once the player leaves the campfire
            _isRange = false;
            _tpMenu.SetActive(false);
        }
        else if (collision.CompareTag("ObjectInteract"))
        {
            _isRangeObject = false;
        }
        else if (collision.tag == "Lavadamage")
        {
            isInLava = false;
        }
    }

    // private void OnTriggerStay2D(Collider2D collision)
    // {
    //     if (_isRange)
    //     {
    //         _tpMenu.SetActive(true);
    //     }
    // }
    
    /// <summary>
    /// Animation for hide/show indication for "campfire activate"
    /// </summary>
    /// <returns></returns>

    private IEnumerator AnimationIndicator()
    {
        Animator indicAnimation = _indicator2.GetComponent<Animator>();
        indicAnimation.SetTrigger("Show");
        yield return new WaitForSeconds(_indicatorDelay);
        indicAnimation.SetTrigger("Hide");
        yield return new WaitForSeconds(_indicatorDelay);
        indicAnimation.SetTrigger("Idle");
        yield return new WaitForSeconds(_indicatorDelay);
    }
    /// <summary>
    /// Animation for hide/show indication "save..."
    /// </summary>
    /// <returns></returns>

    private IEnumerator AnimationSave()
    {
        Animator indicAnimation = _indicatorSave.GetComponent<Animator>();
        indicAnimation.SetTrigger("Show");
        yield return new WaitForSeconds(_indicatorDelaySave);
        indicAnimation.SetTrigger("Hide");
        yield return new WaitForSeconds(_indicatorDelaySave);
        indicAnimation.SetTrigger("Idle");
        yield return new WaitForSeconds(0.1f);
    }
}
