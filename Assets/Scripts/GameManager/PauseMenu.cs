using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    public GameObject m_settingMenu;

    public void ResumeGame()
    {
        GameManager.instance.SetPause(false);
        GameManager.instance.GetMenuManager().CloseMenu();
        GameManager.instance.m_menuManager.ChangeColorBackground(Color.black, 0f);
    }

    public void SettingMenu()
    {
        GameManager.instance.GetMenuManager().OpenMenuByName("Settings");
        GameManager.instance.GetMenuManager().GetMenuByName("Settings").GetComponent<MenuSettings>().OpenSubMenuSettingsGameSettings();
    }

    public void BestiaryMenu()
    {
        GameManager.instance.GetMenuManager().OpenMenuByName("Bestiary");
    }

    public void InventoryMenu()
    {
        GameManager.instance.GetMenuManager().OpenMenuByName("Inventory");
    }

    public void QuitButton()
    {
        // save any game data here
        #if UNITY_EDITOR
            // Application.Quit() does not work in the editor so
            // UnityEditor.EditorApplication.isPlaying need to be set to false to end the game
            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Application.Quit();
        #endif
    }
}
