using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


/// <summary>
/// Class to cancel event drag on scroll rect attached to script
/// </summary>
public class CancelDrag : MonoBehaviour, IDragHandler
{

    public void OnDrag(PointerEventData eventData)
    {
        eventData.pointerDrag = null;
    }

}
