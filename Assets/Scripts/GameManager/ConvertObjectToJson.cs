using System.Collections;
using System.Collections.Generic;
using System.IO;

/// <summary>
/// Class convert string 
/// </summary>
public class ConvertObjectToJson
{
    private static string m_extentionFile = ".json";
    
    /// <summary>
    /// Create a json file with content passed in param
    /// </summary>
    /// <param name="content">content of future file</param>
    /// <param name="pathFile">path of future file</param>
    public static void WriteJsonFile(string content,string pathFile)
    {
        pathFile += m_extentionFile;
        if (File.Exists(pathFile + m_extentionFile))
        {
            File.Delete(pathFile);
        }
        using (StreamWriter writer = new StreamWriter(pathFile))
        {
            writer.Write(content);
            writer.Close();
        }
    }

    /// <summary>
    /// Read Value content in file and return his content
    /// </summary>
    /// <param name="pathFile">path of target file</param>
    /// <returns> content file</returns>
    public static string ReadJsonFile(string pathFile)
    {
        pathFile += m_extentionFile;
        string s;
        using (StreamReader reader = new StreamReader(pathFile))
        {
            s = reader.ReadToEnd();
            reader.Close();
            return s;
        }
    }

    /// <summary>
    /// Search file with path file
    /// </summary>
    /// <param name="pathFile">path of target file</param>
    /// <returns>true if file exist</returns>
    public static bool SearchFile(string pathFile)
    {
        if(File.Exists(pathFile + m_extentionFile))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
