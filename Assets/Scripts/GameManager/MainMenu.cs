﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    private string m_saveDataLink;
    public GameObject m_continueGameBtn;
    public MenuManager m_menuManager;
    private void Awake()
    {
    }

    // Start is called before the first frame update
    void Start()
    {
        m_saveDataLink = Application.persistentDataPath + "/save.txt";
        m_menuManager.OpenMenuByName("Main");
        if (File.Exists(m_saveDataLink))
        {
            m_continueGameBtn.SetActive(true);
        }
        else
        {
            m_continueGameBtn.SetActive(false);
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void NewGame()
    {
        SceneManager.LoadScene("Level 0");
        m_menuManager.CloseMenu();
    }

    public void OpenSettingsMenu()
    {
        m_menuManager.OpenMenuByName("Settings");
        m_menuManager.GetMenuByName("Settings").GetComponent<MenuSettings>().OpenSubMenuSettingsGameSettings();
    }
}
