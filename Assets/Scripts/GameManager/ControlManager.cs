using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class ControlManager : MonoBehaviour
{
    private InputManager m_inputManager;
    private InputManager m_currentInputManager;
    private string m_jsonInputFileDefault;
    private string m_jsonInputFileUser;
    private Vector2 m_inputDirection;

    void Awake()
    {
        m_inputManager = new InputManager();
        m_currentInputManager = new InputManager();
        m_jsonInputFileDefault = Application.persistentDataPath + "/DefaultInputSettings";
        m_jsonInputFileUser = Application.persistentDataPath + "/UserInputSettings";
        if (ConvertObjectToJson.SearchFile(m_jsonInputFileUser))
        {
            string jsonContent = ConvertObjectToJson.ReadJsonFile(m_jsonInputFileUser);
            SetInputManager(jsonContent);
        }
        else if (ConvertObjectToJson.SearchFile(m_jsonInputFileDefault + Application.systemLanguage.ToString()))
        {
            string jsonContent = ConvertObjectToJson.ReadJsonFile(m_jsonInputFileDefault + Application.systemLanguage.ToString());
            SetInputManager(jsonContent);
        }
        else
        {
            CreateDefaultsInputFile();
        }
    }


    private void Update()
    {
        SetInputDirection();
    }

    /// <summary>
    /// Get Vector2 input movement
    /// </summary>
    /// <returns>Vector2 m_inputDirection</returns>
    public void SetInputDirection()
    {
        if (Input.GetKey(m_inputManager.GetForwardKey()))
        {
            m_inputDirection.y = 1;

        }else if (Input.GetKey(m_inputManager.GetBackKey()))
        {
            m_inputDirection.y = -1;
        }

        if (Input.GetKey(m_inputManager.GetLeftKey()))
        {
            m_inputDirection.x = -1;
        }
        else if (Input.GetKey(m_inputManager.GetRightKey()))
        {
            m_inputDirection.x = 1;
        }


        if(Input.GetKey(m_inputManager.GetForwardKey()) == false && Input.GetKey(m_inputManager.GetBackKey()) == false)
        {
            m_inputDirection.y = 0;
        }

        if (Input.GetKey(m_inputManager.GetLeftKey()) == false&& Input.GetKey(m_inputManager.GetRightKey()) == false)
        {
            m_inputDirection.x = 0;
        }

    }

    public Vector2 GetInputDirection() { return m_inputDirection; }

    /// <summary>
    /// detect if player press interact key
    /// </summary>
    /// <returns>boolean</returns>
    public bool InteractInputPressed()
    {
        if (Input.GetKeyDown(m_inputManager.GetInteractKey())) // if player press interact key
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public InputManager GetInputManager()
    {
        return m_inputManager;
    }

    /// <summary>
    /// detect if player press jump key
    /// </summary>
    /// <returns>boolean</returns>
    public bool JumpInputPressed()
    {
        if (Input.GetKeyDown(m_inputManager.GetJumpKey()))
        {
            return true;
        }
        else
        {
            return false;
        }
    }    
    public bool InventoryInputPressed()
    {
        if (Input.GetKeyDown(m_inputManager.GetInventoryKey()))
        {
            return true;
        }
        else
        {
            return false;
        }
    }    
    public bool RunInputPressed()
    {
        if (Input.GetKeyDown(m_inputManager.GetRunKey()))
        {
            return true;
        }
        else
        {
            return false;
        }
    }



    public bool LeftMouseButtonPressed()
    {
        if (Input.GetMouseButtonDown(m_inputManager.GetIntButtonLeft()))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool RightMouseButtonPressed()
    {
        if (Input.GetMouseButtonDown(m_inputManager.GetIntButtonRight()))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool MiddleMouseButtonPressed()
    {
        if (Input.GetMouseButtonDown(m_inputManager.GetIntButtonMiddle()))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool ButtonPauseIsPressed()
    {
        if (Input.GetKeyDown(m_inputManager.GetPauseKey()))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void SetInputManager(string jsonContent)
    {
        m_inputManager = JsonUtility.FromJson<InputManager>(jsonContent);
        m_currentInputManager = JsonUtility.FromJson<InputManager>(jsonContent);
    }

    /// <summary>
    /// Method reset InputManager befor change settings (none save)
    /// </summary>
    public void ChargeInputManager()
    {
        m_inputManager = m_currentInputManager;
    }

    /// <summary>
    /// Method reset inputManager at state in default
    /// </summary>
    public void DefaultInputManager()
    {
        if (Application.systemLanguage == SystemLanguage.French)
        {
            string jsonContent = ConvertObjectToJson.ReadJsonFile(m_jsonInputFileDefault + Application.systemLanguage.ToString());
            SetInputManager(jsonContent);
        }else if (Application.systemLanguage == SystemLanguage.English)
        {
            string jsonContent = ConvertObjectToJson.ReadJsonFile(m_jsonInputFileDefault + Application.systemLanguage.ToString());
            SetInputManager(jsonContent);
        }
        else
        {
            Debug.LogError("Error no default setting found");
        }
    }

    /// <summary>
    /// Create default setting by language system
    /// </summary>
    private void CreateDefaultsInputFile()
    {
        m_inputManager = new InputManager();
        if(Application.systemLanguage == SystemLanguage.French)
        {
            m_inputManager.SetBackKey(KeyCode.S);
            m_inputManager.SetForwardKey(KeyCode.Z);
            m_inputManager.SetLeftKey(KeyCode.Q);
            m_inputManager.SetRightKey(KeyCode.D);
            m_inputManager.SetInteractKey(KeyCode.E);
        }else if(Application.systemLanguage == SystemLanguage.English)
        {
            m_inputManager.SetBackKey(KeyCode.W);
            m_inputManager.SetForwardKey(KeyCode.S);
            m_inputManager.SetLeftKey(KeyCode.A);
            m_inputManager.SetRightKey(KeyCode.D);
            m_inputManager.SetInteractKey(KeyCode.F);
        }
        else
        {
            m_inputManager.SetBackKey(KeyCode.W);
            m_inputManager.SetForwardKey(KeyCode.S);
            m_inputManager.SetLeftKey(KeyCode.A);
            m_inputManager.SetRightKey(KeyCode.D);
            m_inputManager.SetInteractKey(KeyCode.F);
        }
        m_inputManager.SetInventoryKey(KeyCode.I);
        m_inputManager.SetRunKey(KeyCode.LeftShift);
        m_inputManager.SetPauseKey(KeyCode.Escape);
        m_inputManager.SetJumpKey(KeyCode.Space);
        ConvertObjectToJson.WriteJsonFile(JsonUtility.ToJson(m_inputManager), m_jsonInputFileDefault + Application.systemLanguage.ToString());
        string content = ConvertObjectToJson.ReadJsonFile(m_jsonInputFileDefault + Application.systemLanguage.ToString());
        SetInputManager(content);
    }

    /// <summary>
    /// Method to save personal settings user
    /// </summary>
    public void SaveSettings()
    {
        ConvertObjectToJson.WriteJsonFile(JsonUtility.ToJson(m_inputManager),m_jsonInputFileUser); // transform m_inputPlayer Object into json object
        //File.WriteAllText(m_jsonInputFileUser, newJsonFileUser); // create,write and close jsonFile user settings
        string jsonContent = ConvertObjectToJson.ReadJsonFile(m_jsonInputFileUser); // pick data into json file
        SetInputManager(jsonContent); // set inputPlayer with static inputPlayer
    }
}
