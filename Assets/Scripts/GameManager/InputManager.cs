using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MouseButton
{
    None = 5,
    LeftButton = 0,
    RightButton = 1,
    MiddleButton = 2,
}
public class InputManager
{
    public KeyCode m_keyForward;
    public KeyCode m_keyBack;
    public KeyCode m_keyLeft;
    public KeyCode m_keyRight;
    public KeyCode m_keyJump;
    public KeyCode m_keyInteract;
    public KeyCode m_keyPause;
    public KeyCode m_keyInventory;
    public KeyCode m_keyRun;

    public MouseButton m_buttonLeft = MouseButton.LeftButton;
    public MouseButton m_buttonRight = MouseButton.RightButton;
    public MouseButton m_buttonMiddle = MouseButton.MiddleButton;


    #region Setter
    public void SetForwardKey(KeyCode keyCode)
    {
        m_keyForward = keyCode;
    }    
    public void SetBackKey(KeyCode keyCode)
    {
        m_keyBack = keyCode;
    }    
    public void SetLeftKey(KeyCode keyCode)
    {
        m_keyLeft = keyCode;
    }    
    public void SetRightKey(KeyCode keyCode)
    {
        m_keyRight = keyCode;
    }
    public void SetInteractKey(KeyCode keyCode)
    {
        m_keyInteract = keyCode;
    }    
    public void SetJumpKey(KeyCode keyCode)
    {
        m_keyJump = keyCode;
    }    
    public void SetInventoryKey(KeyCode keyCode)
    {
        m_keyInventory = keyCode;
    }    
    public void SetRunKey(KeyCode keyCode)
    {
        m_keyRun = keyCode;
    }    
    public void SetPauseKey(KeyCode keyCode)
    {
        m_keyPause = keyCode;
    }
    #endregion

    #region Getter
    public KeyCode GetForwardKey()
    {
        return m_keyForward;
    } 
    public KeyCode GetBackKey()
    {
        return m_keyBack;
    }
    public KeyCode GetLeftKey()
    {
        return m_keyLeft;
    } 
    public KeyCode GetRightKey()
    {
        return m_keyRight;
    }  
    public KeyCode GetInteractKey()
    {
        return m_keyInteract;
    }    
    public KeyCode GetJumpKey()
    {
        return m_keyJump;
    }
    public KeyCode GetPauseKey()
    {
        return m_keyPause;
    }    
    public KeyCode GetInventoryKey()
    {
        return m_keyInventory;
    }    
    public KeyCode GetRunKey()
    {
        return m_keyRun;
    }
    public MouseButton GetButtonMouse(int button)
    {
        if (button == 0)
        {
            return MouseButton.LeftButton;
        }
        else if (button == 1)
        {
            return MouseButton.RightButton;
        }
        else if (button == 2)
        {
            return MouseButton.MiddleButton;
        }
        else
        {
            return MouseButton.None;
        }
    }

    public string GetKeyName(KeyCode key)
    {
        return key.ToString();
    }

    public string GetKeyName(string keyAction)
    {
        switch (keyAction)
        {
            case "Top":
                return m_keyForward.ToString();
            case "Back":
                return m_keyBack.ToString();
            case "Left":
                return m_keyLeft.ToString();
            case "Right":
                return m_keyRight.ToString();
            case "Interact":
                return m_keyInteract.ToString();
            case "Jump":
                return m_keyJump.ToString();            
            case "Inventory":
                return m_keyInventory.ToString();            
            case "Run":
                return m_keyRun.ToString();
            default:
                return "None";
        }
    }

    public string GetMouseName(MouseButton button)
    {
        return button.ToString();
    }
    public int GetIntButtonLeft()
    {
        return (int)m_buttonLeft;
    }    
    public int GetIntButtonMiddle()
    {
        return (int)m_buttonMiddle;
    }
    public int GetIntButtonRight()
    {
        return (int)m_buttonRight;
    }
    #endregion

}
