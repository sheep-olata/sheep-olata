using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    public GameObject m_parentMenu; // panel regroup all menus of game like child
    public GameObject m_parentSubMenuSetting; // panel regroup all menus of game like child
    public List<GameObject> m_menus; // list regroup all menus

    [SerializeField] private List<string> m_currentMenuPath;

    public void Awake()
    {
        ChangeColorBackground(0, 0, 0, 0f);
        InitMenusArray();
    }
    #region init array menu
    
    /// <summary>
    /// Method to initiate menu array
    /// </summary>
    private void InitMenusArray()
    {
        for (int i = 0; i < m_parentMenu.transform.childCount; i++)
        {
            m_menus.Add(m_parentMenu.transform.GetChild(i).gameObject);
        }
    }
    #endregion

    #region Get Menu

    /// <summary>
    /// Method to get Menu by his name 
    /// nb: name your menu like this "Menu" with following words functionnality example: MenuSettings
    /// </summary>
    /// <param name="menuName"></param>
    /// <returns></returns>
    public GameObject GetMenuByName(string menuName)
    {
        foreach (GameObject menu in m_menus)
        {
            if (menu.name.Contains(menuName))
            {
                return menu;
            }
        }
        return null;
    }
    /// <summary>
    /// Method to get submenu by his name
    /// </summary>
    /// <param name="menuName"></param>
    /// <param name="subMenuList"></param>
    /// <returns></returns>
    public GameObject GetSubMenuByName(string menuName, List<GameObject> subMenuList)
    {
        foreach (GameObject subMenu in subMenuList)
        {
            if (subMenu.name.Contains(menuName))
            {
                return subMenu;
            }
        }
        return null;
    }
    #endregion

    #region Menu path
    /// <summary>
    /// Method to add menu name at path
    /// </summary>
    /// <param name="name"></param>
    public void AddMenuToPath(string name)
    {
        if (!DetectRedondanceOnPath(name))
        {
            m_currentMenuPath.Add(name);
        }
    }

    private bool DetectRedondanceOnPath(string name)
    {
        foreach (string menu in m_currentMenuPath)
        {
            if (menu == name)
            {
                return true;
            }
        }
        return false;
    }


    /// <summary>
    /// Method to remove menu name at path
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public bool RemoveMenuToPath(string name)
    {

        if(m_currentMenuPath.Remove(name))
        {
            return true;
        }else
        {
            return false;
        }
    }
    
    public string GetLastMenuOpen()
    {
        if(m_currentMenuPath.Count != 0)
        {
            return m_currentMenuPath[m_currentMenuPath.Count - 1];
        }
        else
        {
            return "";
        }
    }
    #endregion

    #region Manage menus
    /// <summary>
    /// Method to activate a menu whith his name and add his name into path
    /// </summary>
    /// <param name="menuName"></param>
    /// <returns></returns>
    public bool ActivateMenuByName(string menuName)
    {
        GameObject menu = GetMenuByName(menuName);
        if(menu != null) // if is a menu
        {
            menu.SetActive(true); // activate menu
            AddMenuToPath(menuName); // add name to path menu
            return true; // return true
        }
        else // if is not a menu
        {
            Debug.Log($"none menu found with name: {menuName}");// show error
            return false;
        }
    }
    public bool DisableMenuByName(string menuName,string pos)
    {
        GameObject menu = GetMenuByName(menuName);
        if(menu != null)
        {
            menu.SetActive(false);
            if(pos == "back")
            {
                RemoveMenuToPath(menuName);
            }
            return true;
        }
        else
        {
            return false;
        }
    }
    #endregion

    #region Function to open/close menu by Menu type
    /// <summary>
    /// Method to activate menu
    /// </summary>
    /// <param name="name"></param>
    public void OpenMenuByName(string name)
    {
        if(GetLastMenuOpen()!= "" && name != "Main" || name != "Pause")
        {
            DisableMenuByName(GetLastMenuOpen(),"move");
        }
        ActivateMenuByName(name);
    }
    
    /// <summary>
    /// method to close menu
    /// </summary>
    /// <returns></returns>
    public bool CloseMenu()
    {
        if(GetMenuByName(GetLastMenuOpen()) != null) // if last menu is a menu and not a submenu
        {
            DisableMenuByName(GetLastMenuOpen(), "back"); // disable his
            if(GetLastMenuOpen() != "")
            {
                ActivateMenuByName(GetLastMenuOpen()); // activate predecesor
            }
            return true;
        }
        else
        {
            return false;
        }
    }
    #endregion

    #region Function change Background
    /// <summary>
    /// method change background of parent menu panel
    /// </summary>
    /// <param name="r">Red color intencity</param>
    /// <param name="g">Green color intencity</param>
    /// <param name="b">Blue color intencity</param>
    /// <param name="alpha">Opacity</param>
    public void ChangeColorBackground(float r, float g, float b, float alpha)
    {
        m_parentMenu.GetComponent<Image>().color = new Color(r, g, b, alpha);
    }

    /// <summary>
    /// method change background of parent menu panel
    /// </summary>
    /// <param name="color">Object color</param>
    /// <param name="alpha">Opacity</param>
    public void ChangeColorBackground(Color color, float alpha)
    {
        m_parentMenu.GetComponent<Image>().color = new Color(color.r, color.g, color.b, alpha);
    }
    #endregion
}