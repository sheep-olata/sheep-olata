using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlSettings : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> m_btns;
    [SerializeField]
    private List<GameObject> m_btnsWarn;
    [SerializeField]
    private GameObject m_panelWarn;
    private GameObject m_currentBtn;
    private string m_touchMemory;
    [SerializeField]
    private string m_keyTarget;
    [SerializeField]
    private bool m_isModified = false;
    [SerializeField]
    private GameObject m_textWarn;
    [SerializeField]
    private ScrollRect m_scrollText;
    [SerializeField]
    private GameObject m_content;
    [SerializeField]
    private GameObject m_contentIpnut;

    private MenuSettings m_menuSettings;
    void Start()
    {
        m_menuSettings = transform.parent.parent.gameObject.GetComponent<MenuSettings>();
        m_panelWarn.SetActive(false);
        for (int i = 0; i < m_scrollText.transform.childCount; i++)
        {
            if (m_scrollText.transform.GetChild(i).name.Contains("Content"))
            {
                m_content = m_scrollText.transform.GetChild(i).gameObject;
            }
        }
    }

    private void OnGUI()
    {
        Event e = Event.current;
        if (m_currentBtn != null && e.isKey)
        {
            if (e.keyCode != KeyCode.None && e.keyCode != KeyCode.Escape)
            {
                m_currentBtn.transform.GetChild(0).GetComponent<Text>().text = e.keyCode.ToString();
                SetInputKey(m_keyTarget, e.keyCode);
                GameManager.instance.m_menuManager.GetMenuByName("Settings").GetComponent<MenuSettings>().SetChangeStatus(true);
                BlurButton();
                m_touchMemory = "";
                //GameManager.instance.m_menuManager.CloseSpecificMenu("Settings",true);
            }
        }
    }
    private void Update()
    {
        if(GameManager.instance.m_controller.ButtonPauseIsPressed()&&GameManager.instance.m_menuManager.GetLastMenuOpen() == "change")
        {
            //CancelChangeBtn();
        }
    }

    /// <summary>
    /// Method attached to button in SettingMenu
    /// </summary>
    /// <param name="_btn">Button selected</param>
    /// <param name="_actionName">Action Button</param>
    private void ChangeInput(GameObject _btn,string _actionName)
    {
        if (m_currentBtn == null) // if none button is reference
        {
            m_currentBtn = _btn; // reference to button in parameter
            m_keyTarget = _actionName; // action input button
            FocuButton();
            m_touchMemory = _btn.transform.GetChild(0).GetComponent<Text>().text; //take in memory key in action
            _btn.transform.GetChild(0).GetComponent<Text>().text = ""; // set text in button to void string
            //GameManager.instance.SetCurrentActiveMenu("Settings - change"); // and change active menu for prenven to badly action
        }
        else
        {
            m_currentBtn.transform.GetChild(0).GetComponent<Text>().text = m_touchMemory;
            BlurButton();
            m_currentBtn = _btn; // change older button select into new
            m_keyTarget = _actionName; // and change action input
        }
    }

    /// <summary>
    /// Method to rebind key depending to action attached to button
    /// </summary>
    /// <param name="_action">Action attached to button select</param>
    /// <param name="_key">Key pressed by user</param>
    private void SetInputKey(string _action, KeyCode _key)
    {
        switch (_action)
        {
            case "Top":
                GameManager.instance.m_controller.GetInputManager().SetForwardKey(_key);
                break;            
            case "Back":
                GameManager.instance.m_controller.GetInputManager().SetBackKey(_key);
                break;            
            case "Left":
                GameManager.instance.m_controller.GetInputManager().SetLeftKey(_key);
                break;            
            case "Right":
                GameManager.instance.m_controller.GetInputManager().SetRightKey(_key);
                break;            
            case "Interact":
                GameManager.instance.m_controller.GetInputManager().SetInteractKey(_key);
                break;            
            case "Jump":
                GameManager.instance.m_controller.GetInputManager().SetJumpKey(_key);
                break;            
            case "Inventory":
                GameManager.instance.m_controller.GetInputManager().SetInventoryKey(_key);
                break;            
            case "Run":
                GameManager.instance.m_controller.GetInputManager().SetRunKey(_key);
                break;
            default:
                break;
        }
    }

    private void FocuButton()
    {
        m_currentBtn.GetComponent<Image>().color = Color.gray;
    }
    private void BlurButton()
    {
        m_currentBtn.GetComponent<Image>().color = Color.white;
        m_currentBtn = null;
    }
    
    /// <summary>
    /// Method to set button in menu setting
    /// </summary>
    public void SetButtons()
    {
        foreach (GameObject btn in m_btns)
        {
            btn.transform.GetChild(0).GetComponent<Text>().text = GameManager.instance.m_controller.GetInputManager().GetKeyName(btn.name.Replace("Btn", ""));
            btn.GetComponent<Button>().onClick.AddListener(() => ChangeInput(btn, btn.name.Replace("Btn", "")));
        }
        for (int i = 0; i < m_panelWarn.transform.childCount; i++)
        {
            if (m_panelWarn.transform.GetChild(i).name.Contains("Btn"))
            {
                m_btnsWarn.Add(m_panelWarn.transform.GetChild(i).gameObject);
            }else if (m_panelWarn.transform.GetChild(i).name.Contains("Text"))
            {
                m_textWarn = m_panelWarn.transform.GetChild(i).gameObject;
            }
        }
    }
    public void MultiScroll(Vector2 vector)
    {
        m_content.GetComponent<RectTransform>().position = new Vector3(m_content.GetComponent<RectTransform>().position.x, m_contentIpnut.GetComponent<RectTransform>().position.y, m_content.GetComponent<RectTransform>().position.z);
    }
}
