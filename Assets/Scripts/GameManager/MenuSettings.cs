﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuSettings : MonoBehaviour
{
    public GameObject m_subSettingsMenus;
    public List<GameObject> m_subMenus;

    [SerializeField]
    private List<GameObject> m_btnsWarn;
    private GameObject m_currentSubMenu;
    private MenuManager m_menuManager;
    public GameObject m_panelWarn;
    public Text m_textWarn;
    [SerializeField] private bool m_isChange = false;


    // Start is called before the first frame update
    void Awake()
    {
        InitSubMenuArray();
        if(GameManager.instance == null)
        {
            m_menuManager = GameObject.Find("GameManager").GetComponent<MenuManager>();
        }
        else
        {
            m_menuManager = GameManager.instance.m_menuManager;
        }
        for (int i = 0; i < GameManager.instance.m_menuManager.GetSubMenuByName("Warn", m_subMenus).transform.childCount; i++)
        {
            GameObject child = GameManager.instance.m_menuManager.GetSubMenuByName("Warn", m_subMenus).transform.GetChild(i).gameObject;
            if (child.name.Contains("Btn"))
            {
                m_btnsWarn.Add(child);
            }
        }
        
    }

    private void Start()
    {
        OpenSubMenuSettingsGameSettings();
    }

    #region pick sub menus on setting menu
    private void InitSubMenuArray()
    {
        for (int i = 0; i < m_subSettingsMenus.transform.childCount; i++)
        {
            m_subMenus.Add(m_subSettingsMenus.transform.GetChild(i).gameObject);
        }
    }
    #endregion

    #region manage sub menus
    private bool ActivateSubMenuByName(string name)
    {
       GameObject submenu = m_menuManager.GetSubMenuByName(name, m_subMenus);
        if(submenu != null)
        {
            submenu.SetActive(true);
            m_menuManager.AddMenuToPath(name);
            return true;
        }
        else
        {
            return false;
        }
    }
    
    private bool DisableSubMenuByName(string name)
    {
        GameObject submenu = m_menuManager.GetSubMenuByName(name, m_subMenus);
        if(submenu != null)
        {
            submenu.SetActive(false);
            m_menuManager.RemoveMenuToPath(name);
            return true;
        }
        else
        {
            return false;
        }
    }
    
    #endregion


    public void OpenSubMenuSettingsControls()
    {
        DisableSubMenuByName(m_menuManager.GetLastMenuOpen());
        ActivateSubMenuByName("Control");
        m_menuManager.GetSubMenuByName("Control",m_subMenus).GetComponent<ControlSettings>().SetButtons();
    }
    public void OpenSubMenuSettingsGameSettings()
    {
        DisableSubMenuByName(m_menuManager.GetLastMenuOpen());
        ActivateSubMenuByName("Game");
    }
    public void OpenSubMenySoundSettings()
    {
        DisableSubMenuByName(m_menuManager.GetLastMenuOpen());
        ActivateSubMenuByName("Sound");
    }

    public void OpenWarnMenu()
    {
        DisableSubMenuByName(m_menuManager.GetLastMenuOpen());
        ActivateSubMenuByName("Warn");
    }

    public void DisableCurrentSubMenu()
    {
        m_currentSubMenu.SetActive(false);
    }


    #region Ask confirmation to user
    public void AskConfirmChange()
    {
        if (m_isChange)
        {
            OpenWarnMenu();
            m_textWarn.GetComponent<Text>().text = "Would you want save changes ?";
            m_btnsWarn[0].GetComponent<Button>().onClick.AddListener(ConfirmChange);
        }
        else
        {
            DisableSubMenuByName(m_menuManager.GetLastMenuOpen());
            DisableMenuSettings();
        }
    }

    public void AskConfirmCancel()
    {
        if (m_isChange)
        {
            OpenWarnMenu();
            m_textWarn.GetComponent<Text>().text = "Would you want cancel changes ?";
            m_btnsWarn[0].GetComponent<Button>().onClick.AddListener(ConfirmCancel);
        }
        else
        {
            DisableSubMenuByName(m_menuManager.GetLastMenuOpen());
            DisableMenuSettings();
        }
    }

    public void AskResetSettings()
    {
        OpenWarnMenu();
        m_textWarn.GetComponent<Text>().text = "Would you want reset by default value ?";
        m_btnsWarn[0].GetComponent<Button>().onClick.AddListener(ConfirmReset);
    }
    #endregion


    #region Confirmation from user

    /// <summary>
    /// Method when player confirm his change settings
    /// </summary>
    public void ConfirmChange()
    {
        GameManager.instance.m_controller.SaveSettings();
        m_panelWarn.SetActive(false);
        m_isChange = false;
        m_btnsWarn[0].GetComponent<Button>().onClick.RemoveListener(ConfirmChange);
        DisableSubMenuByName("Warn");
        DisableMenuSettings();
    }

    /// <summary>
    /// Method when player confirm cancel his new settings
    /// </summary>
    public void ConfirmCancel()
    {
        m_panelWarn.SetActive(false);
        m_isChange = false;
        GameManager.instance.m_controller.ChargeInputManager();
        GameManager.instance.m_menuManager.GetSubMenuByName("Control", m_subMenus).GetComponent<ControlSettings>().SetButtons();
        m_btnsWarn[0].GetComponent<Button>().onClick.RemoveListener(ConfirmCancel);
        DisableSubMenuByName("Warn");
        DisableMenuSettings();
    }

    /// <summary>
    /// Method when player confirm want reset his settings
    /// </summary>
    public void ConfirmReset()
    {
        m_panelWarn.SetActive(false);
        GameManager.instance.m_controller.DefaultInputManager();
        m_isChange = false;
        GameManager.instance.m_menuManager.GetSubMenuByName("Control",m_subMenus).GetComponent<ControlSettings>().SetButtons();
        m_btnsWarn[0].GetComponent<Button>().onClick.RemoveListener(ConfirmReset);
        DisableSubMenuByName("Warn");
        DisableMenuSettings();
    }
    #endregion


    public void DisableMenuSettings()
    {
        m_menuManager.CloseMenu();
    }
    public GameObject GetCurrentSubMenu() { return m_currentSubMenu; }
    public bool GetStatusChange() { return m_isChange; }
    public void SetChangeStatus(bool newStatus)
    {
        m_isChange = newStatus;
    }
}
