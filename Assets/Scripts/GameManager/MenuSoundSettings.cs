﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class MenuSoundSettings : MonoBehaviour
{
    [SerializeField] private float m_masterVolume;
    [SerializeField] private float m_effectVolume;
    [SerializeField] private float m_musicVolume;

    public bool m_enableMasterVolume = true;
    public bool m_enableEffectVolume = true;
    public bool m_enableMusicVolume = true;

    public GameObject m_slidersgroup;
    public GameObject m_togglesGroup;

    public AudioMixer m_audioMixer;

    private void Awake()
    {
        InitiateSliders();
        InitiateToggles();
    }

    #region Initiator

    /// <summary>
    /// Method initiate all values channel of audio mixer and applys this value on slider associate
    /// </summary>
    private void InitiateSliders()
    {
        m_audioMixer.GetFloat("Master", out m_masterVolume);
        GetSliderByChannel("Master").value = m_masterVolume;

        m_audioMixer.GetFloat("Effect", out m_effectVolume);
        GetSliderByChannel("Effect").value = m_effectVolume;

        m_audioMixer.GetFloat("Music", out m_musicVolume);
        GetSliderByChannel("Music").value = m_musicVolume;
    }

    /// <summary>
    /// Method take all child in parent and take toggle component
    /// </summary>
    private void InitiateToggles()
    {
        for (int i = 0; i < m_togglesGroup.transform.childCount; i++)
        {
            GameObject child = m_togglesGroup.transform.GetChild(i).gameObject;
            if (child.name.Contains("MasterVolume"))
            {
                SetToggle("Master", m_enableMasterVolume);
            }
            else if (child.name.Contains("EffectVolume"))
            {
                SetToggle("Effect", m_enableEffectVolume);
            }
            else if (child.name.Contains("MusicVolume"))
            {
                SetToggle("Music", m_enableMusicVolume);
            }
        }
    }
    #endregion


    #region method attached directly by editor
    /// <summary>
    /// Set volume at channel name "master" and attached in slider VolumeMaster directely in editor
    /// </summary>
    /// <param name="value"></param>
    
    public void SetVolumeMaster(float value)
    {
        m_masterVolume = Mathf.Round(value);
        if (m_enableMasterVolume)
        {
            SetVolumeByChannel("Master", m_masterVolume);
        }
    }
    /// <summary>
    /// Set volume at channel name "effect" and attached in slider VolumeEffect directely in editor
    /// </summary>
    /// <param name="value"></param>
    
    public void SetVolumeEffect(float value)
    {
        m_effectVolume = Mathf.Round(value);
        if (m_enableEffectVolume)
        {
            SetVolumeByChannel("Effect", m_effectVolume);
        }
    }
    /// <summary>
    /// Set volume at channel name "music" and attached in slider VolumeMusic directely in editor
    /// </summary>
    /// <param name="value"></param>
    
    public void SetVolumeMusic(float value)
    {
        m_musicVolume = Mathf.Round(value);
        if (m_enableMusicVolume)
        {
            SetVolumeByChannel("Music", m_musicVolume);
        }
    }


    public void ToggleMasterVolume(bool newBool)
    {
        m_enableMasterVolume = newBool;
        if (!m_enableMasterVolume)
        {
            SetVolumeByChannel("Master", -80f);
            SetVolumeByChannel("Effect", -80f);
            SetVolumeByChannel("Music", -80f);
            SetToggle("All", false);
            SetInteractiveToggle("All", newBool,true,"Master");
        }
        else
        {
            SetVolumeByChannel("Master", m_masterVolume);
            SetVolumeByChannel("Effect", m_effectVolume);
            SetVolumeByChannel("Music", m_musicVolume);
            SetToggle("All", true);
            SetInteractiveToggle("All", newBool,true,"Master");
        }
    }

    public void ToggleEffectVolume(bool newBool)
    {
        m_enableEffectVolume = newBool;
        if (!m_enableEffectVolume)
        {
            SetVolumeByChannel("Effect", -80f);
            SetInteractiveSlider("Effect", newBool);
        }
        else
        {
            SetVolumeByChannel("Effect", m_effectVolume);
            SetInteractiveSlider("Effect", newBool);
        }
    }
    public void ToggleMusicVolume(bool newBool)
    {
        m_enableMusicVolume = newBool;
        if (!m_enableMusicVolume)
        {
            SetVolumeByChannel("Music", -80f);
            SetInteractiveSlider("Music", newBool);
        }
        else
        {
            SetVolumeByChannel("Music", m_musicVolume);
            SetInteractiveSlider("Music", newBool);
        }
    }
    #endregion

    public void SetVolumeByChannel(string channelName,float volume)
    {
        m_audioMixer.SetFloat(channelName, volume);
    }
    public void SetToggle(string name, bool newBool)
    {
        switch (name)
        {
            case "Master":
                GetToggleByChannel(name).isOn = newBool;
                SetInteractiveSlider(name, newBool);
                break;
            case "Effect":
                GetToggleByChannel(name).isOn = newBool;
                SetInteractiveSlider(name, newBool);
                break;
            case "Music":
                GetToggleByChannel(name).isOn = newBool;
                SetInteractiveSlider(name, newBool);
                break;
            case "All":
                for (int i = 0; i < m_togglesGroup.transform.childCount; i++)
                {
                    m_togglesGroup.transform.GetChild(i).GetComponent<Toggle>().isOn = newBool;
                }
                SetInteractiveSlider(name, newBool);
                break;
            default:
                Debug.LogError($"No toggle with name: {name} found");
                break;
        }
    }
    public void SetInteractiveSlider(string channel,bool newBool)
    {
        switch (channel)
        {
            case "Master":
                GetSliderByChannel(channel).interactable = newBool;
                break;
            case "Effect":
                GetSliderByChannel(channel).interactable = newBool;
                break;
            case "Music":
                GetSliderByChannel(channel).interactable = newBool;
                break;
            case "All":
                for (int i = 0; i < m_slidersgroup.transform.childCount; i++)
                {
                    m_slidersgroup.transform.GetChild(i).GetComponent<Slider>().interactable = newBool;
                }
                break;
            default:
                break;
        }
    }
    
    /// <summary>
    /// Method to enable or disable interactible toggle component
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="newBool"></param>
    /// <param name="channelIgnore">optionnal parameter trigger if you want ignore a channel</param>
    /// <param name="channelName">optionnal parametter give you channel name ignore</param>
    public void SetInteractiveToggle(string channel, bool newBool, bool channelIgnore = false,string channelName = "none")
    {
        switch (channel)
        {
            case "Master":
                GetToggleByChannel(channel).interactable = newBool;
                break;
            case "Effect":
                GetToggleByChannel(channel).interactable = newBool;
                break;
            case "Music":
                GetToggleByChannel(channel).interactable = newBool;
                break;
            case "All":
                if (channelIgnore)
                {
                    if (channelName.Equals("none"))
                    {
                        Debug.LogWarning("None channel name ignore");
                    }
                    else
                    {
                        for (int i = 0; i < m_togglesGroup.transform.childCount; i++)
                        {
                            GameObject toggle = m_togglesGroup.transform.GetChild(i).gameObject;
                            if (!toggle.name.Contains(channelName))
                            {
                                toggle.GetComponent<Toggle>().interactable = newBool;
                            }
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < m_togglesGroup.transform.childCount; i++)
                    {
                        m_togglesGroup.transform.GetChild(i).GetComponent<Toggle>().interactable = newBool;
                    }
                }
                break;
            default:
                break;
        }
    }

    public Slider GetSliderByChannel(string channel)
    {
        for (int i = 0; i < m_slidersgroup.transform.childCount; i++)
        {
            GameObject slider = m_slidersgroup.transform.GetChild(i).gameObject;
            if (slider.name.Contains(channel))
            {
                return slider.GetComponent<Slider>();
            }
        }
        Debug.LogError($"None GameObject or slider component found by this channel {channel}");
        return null;
    }    
    public Toggle GetToggleByChannel(string channel)
    {
        for (int i = 0; i < m_togglesGroup.transform.childCount; i++)
        {
            GameObject toggle = m_togglesGroup.transform.GetChild(i).gameObject;
            if (toggle.name.Contains(channel))
            {
                return toggle.GetComponent<Toggle>();
            }
        }
        Debug.LogError($"None GameObject or toggle component found by this channel {channel}");
        return null;
    }
}
