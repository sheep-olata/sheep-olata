using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ControlManager))]
[RequireComponent(typeof(MenuManager))]
public class GameManager : MonoBehaviour
{
    static public GameManager instance;
    public ControlManager m_controller { get; private set; }
    public MenuManager m_menuManager { get; private set; }

    [SerializeField]
    private bool m_isPaused;

    private void Awake()
    {
        if(instance != null && instance != this)
        {
            Debug.LogError("Instance GameManager is already instanciate !");
            Destroy(this);
        }
        else
        {
            SetPause(false);
            m_controller = GetComponent<ControlManager>();
            m_menuManager = GetComponent<MenuManager>();
            instance = this;
        }
    }

    private void Update()
    {
        if (m_controller.ButtonPauseIsPressed() && m_menuManager.GetLastMenuOpen() == "")
        {
            m_menuManager.OpenMenuByName("Pause");
        }
        else if (m_controller.InventoryInputPressed() && m_menuManager.GetLastMenuOpen() == "")
        {
            m_menuManager.OpenMenuByName("Inventory");
        }
        else if (m_controller.InventoryInputPressed() && m_menuManager.GetLastMenuOpen() == "Inventory")
        {
            m_menuManager.CloseMenu();
        }
        else if (m_controller.ButtonPauseIsPressed() && m_menuManager.GetLastMenuOpen() == "Pause")
        {
            m_menuManager.CloseMenu();
        }
        else if (m_controller.ButtonPauseIsPressed() && m_menuManager.GetLastMenuOpen() == "Settings")
        {
            m_menuManager.CloseMenu();
        }
        else if (m_controller.ButtonPauseIsPressed() && m_menuManager.GetLastMenuOpen() == "Bestiary")
        {
            m_menuManager.CloseMenu();
        }


        if (m_menuManager.GetLastMenuOpen() == "" || m_menuManager.GetLastMenuOpen() == "Main")
        {
            SetPause(false);
            m_menuManager.ChangeColorBackground(0, 0, 0, 0);
        }
        else
        {
            SetPause(true);
            m_menuManager.ChangeColorBackground(0, 0, 0, 0.25f);
        }
        GameIsPaused();
    }

    private void GameIsPaused()
    {
        if (m_isPaused) // if game is paused
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }
    #region Setter
    public void SetPause(bool newBool) { m_isPaused = newBool;}
    //public void SetCurrentActiveMenu(string menuName) { m_currentMenuActive = menuName; }
    #endregion

    #region Getter
    public ControlManager GetControlManager(){ return m_controller; }
    public MenuManager GetMenuManager() { return m_menuManager; }
    //public string GetCurrentActiveMenu() { return m_currentMenuActive; }
    public bool GetIsPaused() { return m_isPaused; }
    #endregion
}
