﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructableRigidBody : MonoBehaviour
{
    [SerializeField]
    Vector2 forceDirection;

    [SerializeField]
    // Torque is a couple of force
    float torque;

    Rigidbody2D rb2d;

    // Start is called before the first frame update
    void Start()
    {
        // We add random number for the torque & force direction
        float randTorque = UnityEngine.Random.Range(-50, 50);
        float randForceX = UnityEngine.Random.Range(forceDirection.x - 50, forceDirection.x + 50);
        float randForceY = UnityEngine.Random.Range(forceDirection.y, forceDirection.y + 50);

        forceDirection.x = randForceX;
        forceDirection.y = randForceY;

        // Get the rigidbody 2D
        rb2d = GetComponent<Rigidbody2D>();
        // We add force and torque
        rb2d.AddForce(forceDirection);
        rb2d.AddTorque(randTorque);

        // We destroy object after a random time
        Invoke("DestroySelf", UnityEngine.Random.Range(2.5f, 4f));
    }

    void DestroySelf()
    {
        Destroy(gameObject);
    }
}
