﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobSpawner : MonoBehaviour
{
    public GameObject _mobPrefab;
    public GameObject _mob;
    public GameObject spawnPoint;
    public bool _mobIsAlive;

    public float cooldownTime;
    public float nextFireTime;

    void Start()
    {
        MobSpawning();
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time > nextFireTime && !_mobIsAlive)
        {
            MobSpawning();
        }
    }

    private void MobSpawning()
    {
        _mob = Instantiate(_mobPrefab);
        _mob.transform.SetParent(spawnPoint.transform);
        _mob.transform.position = spawnPoint.transform.position;
        _mobIsAlive = true;
    }

    public void ActivateCooldown()
    {
        nextFireTime = Time.time + cooldownTime;
    }
}
