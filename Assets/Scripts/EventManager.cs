﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class EventManager : MonoBehaviour
{
    public static EventManager instance;
    public bool loadingGame = false;


    // Start is called before the first frame update
    void Awake()
    {
        if (instance != null)
        {
            Debug.Log("Il y'a plus d'une instance de EventManager dans la scene");
            Destroy(this.gameObject);
            return;
        }
        instance = this;

        SceneManager.sceneLoaded += Onload;
    }

   

    // Update is called once per frame
    void Update()
    {

    }
    private void Onload(Scene arg0, LoadSceneMode arg1)
    {
        if(loadingGame)
        {
            SaveManager.LoadGame();
            Debug.Log("loading a game");
            loadingGame = false;
        }
    }

}
