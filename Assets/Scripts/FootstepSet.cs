﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Scriptable object used to store sets of sounds for footsteps
/// </summary>
[CreateAssetMenu(fileName = "FootstepSet", menuName = "ScriptableObjects/FootstepSets", order = 1)]
public class FootstepSet : ScriptableObject
{
    public string SetName = "defaultname";
    public List<AudioClip> Steps;
    public List<AudioClip> Jumps;
    public List<AudioClip> Lands;


}
