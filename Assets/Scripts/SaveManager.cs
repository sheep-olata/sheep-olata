﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Serialization;




public static class SaveManager
{

    #region I/O functions
    /// <summary>
    /// Read and write function to get saves from text file
    /// </summary>

    public static void WriteString(string s)//writes a string (param s) into a text file in the specified path
    {
        string path = Application.persistentDataPath + "/save.txt";//C:\Users\[User]\AppData\LocalLow\Sheeps Studios\Sheep'Olata
        File.Delete(path);//clears the file to avoid writing data multiple times
        //Write some text to the save.txt file
        StreamWriter writer = new StreamWriter(path, true);
        writer.Write(s);
        writer.Close();


        //StreamReader readerDebug = new StreamReader(path);//outputs the data writen in debug.log
        //Debug.Log(readerDebug.ReadToEnd());

    }
    public static string ReadString()//reads from a text file and returns a string
    {
        string path = Application.persistentDataPath + "/save.txt";
        //Read the text directly from the save.txt file
        StreamReader reader = new StreamReader(path);
        string s = reader.ReadToEnd();//s now contains save.txt
        reader.Close();


        
        //Debug.Log(s);//output s in debug.log

        return s;
    }


    public static void WriteJson(SaveFile savefile)//generates a json from a savefile struct and write it using the write method
    {
        WriteString(JsonUtility.ToJson(savefile));
    }

    public static SaveFile ReadJson()//read textfile with the read method and returns a savefile struct
    {
        return JsonUtility.FromJson<SaveFile>(ReadString());
    }
    #endregion



    public static void SaveGame(PlayerData player, InventaireData inventaire, int _levelToLoad)//saves a SaveFile struct as a Json into a textfile in the project
    {
        SaveFile savefile = new SaveFile(player, inventaire, _levelToLoad);
        WriteJson(savefile);
    }

    public static void LoadGame()//reads from the saved textfile, and instanciate a Savefile Struct from it //TODO -> basic file checks and error catching
    {
        SaveFile savefile = ReadJson();
        Loader loader = new Loader();
        loader.Load(savefile);
        //DO STUFF LMAO
    }
    public static int GetLevelToLoad()
    {
        int _levelToLoad = 0;
        _levelToLoad = ReadJson().levelToLoad;

        return _levelToLoad;
    }

}


public class Loader : MonoBehaviour //loader class that implements monobehaviour to use the findobjectoftype unity func and apply the data contained in the savefile struct to existing objects at runtime
{
    internal void Load(SaveFile savefile)
    {
        FindObjectOfType<Player>().LoadFromPlayerData(savefile.player);
        FindObjectOfType<Inventaire>().LoadFromInventaireData(savefile.inventaire);
    }
}


[System.Serializable]
public struct SaveFile
{
    public SaveFile(PlayerData p, InventaireData i,int _levelToLoad)//constructor to init the struct for serialization
    {
        player = p;
        inventaire = i;
        levelToLoad = _levelToLoad;
        
    }
    public int levelToLoad;
    public PlayerData player;
    public InventaireData inventaire;


}
