﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObject : MonoBehaviour
{
    [SerializeField]
    // The health of this object we want destroy
    int healthObject = 3;

    [SerializeField]
    // The reference to object
    Object destructableRef;

    // Variables that we need to shaking object when we hit them
    bool isShaking = false;
    float shakeAmount = .02f;

    Vector2 startPosition;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (isShaking)
        {
            // We move the position to make shking object when we hit them
            transform.position = startPosition + UnityEngine.Random.insideUnitCircle * shakeAmount;
        }
    }


    public void TakeHit()
    {
        startPosition = transform.position;

        // We hit object
        healthObject--;

        if (healthObject <= 0)
        {
            // Death the object...
            ExplodeThisGameObject();
        }
        else
        {
            // We shaking object
            isShaking = true;
            Invoke("ResetShake", .2f);
        }
    }

    void ResetShake()
    {
        // To reset shaking, we pass the bool to false and replace oblect to the start position
        isShaking = false;
        transform.position = startPosition;
    }

    private void ExplodeThisGameObject()
    {
        GameObject destructable = (GameObject)Instantiate(destructableRef);
        
        //map the newly loaded destructable object to the x and y position of the previouly destroyed object
        destructable.transform.position = transform.position;

        Destroy(gameObject);
    }
}
