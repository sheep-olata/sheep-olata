﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformMovement : MonoBehaviour
{
    public Platform platform;
    public Transform[] waypoints;
    public Transform target;
    public SpriteRenderer grassHalfLeft;
    public SpriteRenderer grassHalfRight;
    public int waypointIndex = 0;
    private float flashPlatform = 0.7f;
    public bool hitPlayer = false;
    private bool warn = false;

    /// <summary>
    /// Au moment de commencer le script on modifie/Set les valeur de la plateforme en fonction du type attribuer depuis Unity
    /// </summary>
    void Start()
    {
        target = waypoints[0];
        platform.SetStat(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 direction = target.position - transform.position;
        transform.Translate(direction.normalized * platform.platformSpeed * Time.deltaTime, Space.World);

        if (Vector3.Distance(transform.position, target.position) < 0.3f)
        {
            waypointIndex = (waypointIndex + 1) % waypoints.Length;
            target = waypoints[waypointIndex];
        }
    }
    /// <summary>
    /// Methode qui permet d'indiquer au programme/jeu qu'un collider est entrer en collision de celui de plateforme en occurence celui du joueur
    /// si la plateforme est de type collante alors on execute les methode pour la plateforme collante
    /// si la plateforme est de type collante alors on execute les methode pour la plateforme glissante
    /// si la plateforme est de type collante alors on execute les methode pour la plateforme disparait
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") && platform._type == "sticky")
        {
            //PlayerMovement.instance.isStiky = true;
            Player.instance.JumpForce /= platform.Sticky;
            Player.instance.MoveSpeed /= platform.Sticky;
        }
        if (collision.CompareTag("Player") && platform._type == "disappear")
        {
            hitPlayer = true;
            GameObject plat = gameObject;
            GameObject player = collision.gameObject;
            StartCoroutine(FlashPlatform());
            StartCoroutine(DisapearPlatform(plat, player));
        }
        if (collision.CompareTag("Player") && platform._type == "slide")
        {
            //PlayerMovement.instance.isSlide = true;
            //PlayerMovement.instance._coefSlidely *= platform.Slide;
        }
    }
    /// <summary>
    /// Methode qui permet d'indiquer au programme/jeu qu'un collider est sortie de celui de plateforme en occurence celui du joueur
    /// si la plateforme est de type collante alors on execute les methode pour la plateforme collante
    /// si la plateforme est de type collante alors on execute les methode pour la plateforme glissante
    /// si la plateforme est de type collante alors on execute les methode pour la plateforme disparait
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") && platform._type == "sticky")
        {
            //PlayerMovement.instance.isStiky = false;
            Player.instance.JumpForce *= platform.Sticky;
            Player.instance.MoveSpeed *= platform.Sticky;
        }
        if (collision.CompareTag("Player") && platform._type == "disappear")
        {
            hitPlayer = false;
        }
        if(collision.CompareTag("Player") && platform._type == "slide")
        {
            //PlayerMovement.instance.isSlide = false;
            //PlayerMovement.instance._coefSlidely /= platform.Slide;
        }
    }
    /// <summary>
    ///  Methode permettant de signaler au joueur que cette plateforme est susceptible de céder/disparaite sous ses pieds
    ///  Dans un premier temps la plateforme clignote avec sa couleur de base puis quand la condition du _warn est active elle clignote en rouge
    /// </summary>
    /// <returns></returns>
    IEnumerator FlashPlatform()
    {
        int i = 0;
        while (i <= 7)
        {
            if (warn)
            {
                grassHalfLeft.color = new Color(1f, 0f, 0f, 0f);
                grassHalfRight.color = new Color(1f, 0f, 0f, 0f);
                yield return new WaitForSeconds(flashPlatform);
                grassHalfLeft.color = new Color(1f, 0f, 0f, 1f);
                grassHalfRight.color = new Color(1f, 0f, 0f, 1f);
                Debug.Log(i);
                i++;
                Debug.Log(i);
                yield return new WaitForSeconds(flashPlatform);
            }
            else
            {
                grassHalfLeft.color = new Color(1f, 1f, 1f, 0f);
                grassHalfRight.color = new Color(1f, 1f, 1f, 0f);
                yield return new WaitForSeconds(flashPlatform);
                grassHalfLeft.color = new Color(1f, 1f, 1f, 1f);
                grassHalfRight.color = new Color(1f, 1f, 1f, 1f);
                Debug.Log(i);
                i++;
                Debug.Log(i);
                yield return new WaitForSeconds(flashPlatform);
            }
        }
    }

    /// <summary>
    /// Methode permettant de faire disparaite physiquement la plateforme puis apres un delais la faire réaparaitre
    /// Dans un premier temps elle ne fait rien pensant 5 secondes.
    /// Dans un second temps elle indique au joueur que la plateforme est sous le point de céder.
    /// Dans un troisièle temps on retire le joueur de l'ancre de la plateforme
    /// Dans un quatrième temps on retire les boxCollider et les 3 éléments visuel de la plateforme
    /// Dans un cinquième temps on re ajoute les boxCollider et les 3 éléments visuel de la plateforme
    /// </summary>
    /// <param name="plat"></param>
    /// <param name="player"></param>
    /// <returns></returns>
    /// 

    IEnumerator DisapearPlatform(GameObject plat, GameObject player)
    {
        while (hitPlayer)
        {
            BoxCollider2D[] colliders = plat.GetComponents<BoxCollider2D>();
            GameObject graphicLeft = plat.transform.Find("grassHalfLeft").gameObject;
            GameObject graphicRight = plat.transform.Find("grassHalfRight").gameObject;

            //GameObject[] graphics = plat
            yield return new WaitForSeconds(5f);
            warn = true;
            yield return new WaitForSeconds(1.5f);
            player.transform.parent = null;
            yield return new WaitForSeconds(.5f);
            foreach (var col in colliders)
            {
                col.enabled = false;
            }
            graphicLeft.SetActive(false);
            graphicRight.SetActive(false);
            warn = false;
            yield return new WaitForSeconds(3f);
            foreach (var col in colliders)
            {
                col.enabled = true;
            }
            graphicLeft.SetActive(true);
            graphicRight.SetActive(true);
            yield return new WaitForSeconds(5f);
        }

    }

}
