﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class Platform
{
    public string _type;
    public float _coeficient = 0f;
    public float platformSpeed;

    private float _sticky { set; get; }
    private float _slide { set; get; }
    private bool _isDissapear { set; get; }


    /// <summary>
    /// fonction qui permet d'initier la plateforme en fonction du type donnée depuis Unity
    /// </summary>
    public void SetStat(GameObject plat)
    {
        switch (_type)
        {
            case "normal":
                Sticky = 0;
                Slide = 0;
                Disapear = false;
                break;
            case "sticky":
                Sticky = 1.25f;
                Slide = 0;
                Disapear = false;
                break;
            case "slide":
                Sticky = 0;
                Slide = 2.5f;
                Disapear = false;
                break;
            case "disappear":
                Sticky = 0;
                Slide = 0;
                Disapear = true;
                break;
            case "bouncie":
                PhysicsMaterial2D physics = new PhysicsMaterial2D();
                physics.bounciness = 1.25f;
                physics.friction = 0f;
                Sticky = 0;
                Slide = 0;
                Disapear = false;
                plat.GetComponent<BoxCollider2D>().sharedMaterial = physics;
                break;
            default:
                break;
        }
    }
    public float Sticky
    {
        set { _sticky = value + _coeficient; }
        get { return _sticky; }
    }
    public float Slide
    {
        set { _slide = value + _coeficient; }
        get { return _slide; }
    }
    public bool Disapear
    {
        set { _isDissapear = value; }
        get { return _isDissapear; }
    }
}
