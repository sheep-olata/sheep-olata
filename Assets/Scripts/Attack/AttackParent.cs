﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public enum AttackType
{
    Area = 0,
    MonoTarget = 1,
}

/// <summary>
/// Master class to attack
/// </summary>
public abstract class AttackParent : MonoBehaviour
{
    protected abstract string m_attackName { get; set; } // name attack
    
    protected abstract string m_describeAttack { get; set; } // describe attack

    protected abstract AttackType m_type {  get; set;} // type attack AOE or mono target
    protected abstract float m_trueDamage {  get; set;} // true damage of attack
    protected abstract bool m_hasCoolDown {  get; set;} // condition if cooldown applied
    protected abstract float m_coolDown { get; set; } // cooldown inter attack for throw a nother 
    public abstract float delay { get; set; } // delay applied for cooldown

    protected abstract float m_damage { get; set; }

    /// <summary>
    /// Init all component and attribute to child class ParentAttack
    /// </summary>
    public abstract void InitAttack(ICharacters character);
    #region Getter
    public string GetAttackName() { return m_attackName; }
    public AttackType GetAttackType() { return m_type; }
    public float GetTrueDamage() { return m_trueDamage; }
    public bool GetHasCoolDown() { return m_hasCoolDown; }
    public float GetCoolDown() { return m_coolDown; }
    public abstract float GetDamage();
    #endregion
    #region Setter

    public abstract void SetDamage(float ownerDamage);
    #endregion
    /// <summary>
    /// Method to attack target
    /// </summary>
    /// <param name="target">target aim to take damage</param>
    public abstract void TakeDamage(Enemie target);
    /// <summary>
    /// method to attack targets group
    /// </summary>
    /// <param name="targets">list target in area effect</param>
    public abstract void TakeDamage(List<Enemie> targets);
    public abstract float SetCoolDown();
}
