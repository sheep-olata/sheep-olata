﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoisonousGas : AttackParent
{
    public override float delay { get; set; }
    protected override string m_attackName { get ; set ; }
    protected override string m_describeAttack { get ; set ; }
    protected override AttackType m_type { get ; set ; }
    protected override float m_trueDamage { get ; set ; }
    protected override bool m_hasCoolDown { get ; set ; }
    protected override float m_coolDown { get ; set ; }
    protected override float m_damage { get ; set ; }

    [SerializeField] private float m_delayInterDamage = 1f;
    [SerializeField] private float m_coolDownInterdamage = 0f;
    public override float GetDamage()
    {
        throw new System.NotImplementedException();
    }

    public override void InitAttack(ICharacters character)
    {
        delay = 6.0f;
        m_attackName = "Poisonus breath";
        m_describeAttack = "Throw up a gas poisoning enemies during .";
        m_type = AttackType.Area;
        m_hasCoolDown = true;
        m_coolDown = SetCoolDown();
        m_trueDamage = 1f;
        m_coolDownInterdamage = m_delayInterDamage + Time.time;
        SetDamage(character.DamagePoint);
    }

    public override float SetCoolDown()
    {
        return delay + Time.time;
    }

    public override void SetDamage(float ownerDamage)
    {
        m_damage = m_trueDamage + ownerDamage;
    }

    public override void TakeDamage(Enemie target)
    {
        throw new System.NotImplementedException();
    }

    private void Update()
    {
        if (Time.time > m_coolDownInterdamage)
        {
            TakeDamage(PickEnemies());
        }
        if (Time.time > m_coolDown)
        {
            Destroy(gameObject);
        }
    }


    public override void TakeDamage(List<Enemie> targets)
    {
        float rateEndCooldown = (m_coolDown - Time.time) / m_coolDown;
        if (rateEndCooldown < 0.3f)
        {
            for (int i = 0; i < targets.Count; i++)
            {
                targets[i].TakeDamage((int)Mathf.Round(m_damage));
            }
        }
        else if (rateEndCooldown > 0.3f && rateEndCooldown < 0.75f)
        {
            for (int i = 0; i < targets.Count; i++)
            {
                targets[i].TakeDamage((int)Mathf.Round(m_damage * 1.25f));
            }
        }
        else if (rateEndCooldown > 0.75f)
        {
            for (int i = 0; i < targets.Count; i++)
            {
                targets[i].TakeDamage((int)Mathf.Round(m_damage * 1.50f));
            }
        }
    }

    public List<Enemie> PickEnemies()
    {
        List<Enemie> enemies = new List<Enemie>();
        RaycastHit2D[] hits = Physics2D.CircleCastAll(transform.position, transform.localScale.x / 2, Vector2.right, 0.5f);

        if (hits.Length != 0)
        {
            for (int i = 0; i < hits.Length; i++)
            {
                if (hits[i].collider != null && hits[i].collider.CompareTag("Enemie"))
                {
                    Enemie enemie;
                    hits[i].collider.TryGetComponent(out enemie);
                    enemies.Add(enemie);
                }
            }
        }
        return enemies;
    }
}
