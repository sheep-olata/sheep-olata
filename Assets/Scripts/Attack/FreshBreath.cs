﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FreshBreath : AttackParent
{
    public override float delay { get ; set ; }
    protected override string m_attackName { get ; set ; }
    protected override AttackType m_type { get ; set ; }
    protected override float m_trueDamage { get ; set ; }
    protected override bool m_hasCoolDown { get ; set ; }
    [SerializeField] protected override float m_coolDown { get ; set ; }
    protected override float m_damage { get ; set ; }
    protected override string m_describeAttack { get ; set ; }

    [SerializeField] private float m_delayInterDamage = 1f;
    [SerializeField] private float m_coolDownInterdamage = 0f;
    public float test;
    public override float GetDamage()
    {
        throw new System.NotImplementedException();
    }

    public override void InitAttack(ICharacters character)
    {
        delay = 5.0f;
        m_attackName = "Fresh Breath";
        m_describeAttack = "After eating grass Olata can throw a fresh breath disturb enemie with slow targets and take damage.";
        m_type = AttackType.Area;
        m_hasCoolDown = true;
        m_coolDown = SetCoolDown();
        test = m_coolDown;
        m_trueDamage = 2f;
        m_coolDownInterdamage = m_delayInterDamage + Time.time;
        SetDamage(character.DamagePoint);
        Player.instance.SetHasEatGrass(false);
    }

    public override float SetCoolDown()
    {
        return delay + Time.time;
    }

    public override void SetDamage(float ownerDamage)
    {
        m_damage = m_trueDamage + ownerDamage;
    }

    public override void TakeDamage(Enemie target)
    {
        throw new System.NotImplementedException();
    }
    public override void TakeDamage(List<Enemie> targets)
    {
        if(targets.Count > 0)
        {
            for (int i = 0; i < targets.Count; i++)
            {
                targets[i].MoveSpeed /= 0.25f;
                targets[i].TakeDamage((int)m_damage);
            }
            m_coolDownInterdamage = m_delayInterDamage + Time.time;
        }
    }

    private void Update()
    {
        if(Time.time > m_coolDownInterdamage)
        {
            TakeDamage(PickEnemies());
        }
        if (Time.time > m_coolDown)
        {
            Destroy(gameObject);
        }

    }
    public List<Enemie> PickEnemies()
    {
        List<Enemie> enemies = new List<Enemie>();
        RaycastHit2D[] hits = Physics2D.CircleCastAll(transform.position, transform.localScale.x / 2, Vector2.right,0.5f);

        if(hits.Length != 0)
        {
            for (int i = 0; i < hits.Length; i++)
            {
                if(hits[i].collider != null && hits[i].collider.CompareTag("Enemie"))
                {
                    Enemie enemie;
                    hits[i].collider.TryGetComponent(out enemie);
                    enemies.Add(enemie);
                }
            }
        }
        return enemies;
    }
}
