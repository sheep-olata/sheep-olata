﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Water : MonoBehaviour
{
    public void OnTriggerEnter2D(Collider2D other)
	{
        if (other.CompareTag("Player")) {
            other.gameObject.GetComponent<PlayerMovement>().m_isSwimming = true;
        }
    }

    public void OnTriggerExit2D(Collider2D other)
	{
        if (other.CompareTag("Player")) {
            other.gameObject.GetComponent<PlayerMovement>().m_isSwimming = false;
        }
    }
}
